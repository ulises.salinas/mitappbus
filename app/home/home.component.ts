import {Component, ElementRef, OnInit, ViewChild, ViewContainerRef} from "@angular/core";
import * as appSettings from "tns-core-modules/application-settings";
import {alert, login} from "tns-core-modules/ui/dialogs";
import { BarcodeScanner, ScanResult } from 'nativescript-barcodescanner';
import { request,  HttpResponse } from "tns-core-modules/http";
import { prompt, PromptResult, PromptOptions, inputType, capitalizationType } from "tns-core-modules/ui/dialogs";

import * as Toast from "nativescript-toast";
import { SelectedIndexChangedEventData, ValueList } from "nativescript-drop-down";
import {ModalDialogService, ModalDialogOptions} from "nativescript-angular/modal-dialog";
import {BanderazoComponent} from "~/banderazo/banderazo.component";




@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    num: string = "";
    str: string = "";
    bool: boolean = false;



    qrCodeScaned: string = "";
    mitabusApiUrlOdz: string = "http://mitabus.com/webservice/getcheck/";
    mitabusApiUrlOtros: string = "http://mitabus.com/webservice/getcheckother/";
    urlEnviada = '';

    items = new ValueList<string>();
    selectedIndex = 1;
    baseElegida: any = null;
    existeBase  = false;
    

    constructor(private barcodeScanner: BarcodeScanner,
                private modalService: ModalDialogService,
                private viewContainerRef: ViewContainerRef) {
    }

    displayPromptDialog() {
        // >> prompt-dialog-code
        /*
        import {
            prompt,
            PromptResult,
            PromptOptions,
            inputType,
            capitalizationType
        } from "tns-core-modules/ui/dialogs";
        */
        let options: PromptOptions = {
            title: "Registrar Otro",
            okButtonText: "Enviar",
            cancelButtonText: "Cancelar",
            cancelable: true,
            inputType: inputType.number, // email, number, text, password, or email
            capitalizationType: capitalizationType.sentences // all. none, sentences or words
        };

        prompt(options).then((result: PromptResult) => {
            if (result.text.length) {
                this.enviarHttpOtro({idBase: this.baseElegida.value, numero: result.text})
            }
        });
        // << prompt-dialog-code
    }

    ngOnInit(): void {
        this.initBases();
        
        // second parameter to getXXX() is a default value
        const num = appSettings.getNumber("someNumber", 0);
        this.num = num === 0 ? "" : num.toString();
        this.str = appSettings.getString("someString", "");
        this.bool = appSettings.getBoolean("someBoolean", false);
       this.leerBase();
    }


    onScan() {

        let scan = () => {
            this.barcodeScanner.scan({
              formats: "QR_CODE, EAN_13",
              beepOnScan: true,
              reportDuplicates: true,
              preferFrontCamera: false
            })
                .then((result: ScanResult) => { 
                    if (result.text.length == 40)  {
                        this.qrCodeScaned = result.text;
                        console.log(JSON.stringify(result));
                        this.enviarHttpOdz({idBase: this.baseElegida.value, qr: result.text});
                    }
                 })
                .catch(error => console.log(error));
          };

          this.barcodeScanner.hasCameraPermission()
          .then(granted => granted ? scan() : console.log("Permission denied"))
          .catch(() => {
            this.barcodeScanner.requestCameraPermission()
                .then(() => scan());
          });
    }

    enviarHttpOdz({ idBase, qr, lat, long }: { idBase: number; qr: string; lat?: number; long?: number }) {
        this.urlEnviada = this.mitabusApiUrlOdz + `${idBase}/${qr}/`;
        request({
            url: this.urlEnviada,
            method: "GET"
        }).then((response: HttpResponse) => {
            // Content property of the response is HttpContent
            // The toString method allows you to get the response body as string.
            const str = response.content.toString();
            // The toJSON method allows you to parse the received content to JSON object
            // var obj = response.content.toJSON();
            // The toImage method allows you to get the response body as ImageSource.
            // var img = response.content.toImage();
            alert(`Petición enviada ${str}` );
        }, (e) => {
        });
    }

    enviarHttpOtro({ idBase, numero, lat, long }: { idBase: number; numero: string;  lat?: number; long?: number }) {
        this.urlEnviada = this.mitabusApiUrlOtros + `${idBase}/${numero}/`;
        request({
            url: this.urlEnviada,
            method: "GET"
        }).then((response: HttpResponse) => {
            // Content property of the response is HttpContent
            // The toString method allows you to get the response body as string.
            const str = response.content.toString();
            // The toJSON method allows you to parse the received content to JSON object
            // var obj = response.content.toJSON();
            // The toImage method allows you to get the response body as ImageSource.
            // var img = response.content.toImage();
            alert(`Petición enviada ${str}` );
        }, (e) => {
        });
    }

    selectedIndexChanged(args: SelectedIndexChangedEventData) {
        this.baseElegida = this.items.getItem(args.newIndex);
    }

    guardarBase() {
        appSettings.setString('labase', JSON.stringify(this.baseElegida));
        Toast.makeText('base: '+ this.baseElegida.display).show();
        this.leerBase();
    }

    initBases() {
        let items = this.makeBases();
        items.forEach(i => {
          this.items.push(i);
        });
      }
      
      makeBases(): {value: string, display: string}[] {
        return [
        { value: '1',  display: "Central Pachuca"},
        { value: '2',  display: "Soriana Del Valle"},
        { value: '5',  display: "Villas de Pachuca"},
        { value: '7',  display: "San Javier"},
        { value: '11', display: "Tizayuca Centro"},
        { value: '4',  display: "Tecamac"},
        { value: '6',  display: "Potrero"},
        { value: '9',  display: "La 3030"},
        { value: '3',  display: "Tizayuca Carpa"},
        // { value: '10', display: "El Carmen"},
        // { value: '8',  display: "18 de Marzo"},
        { value: '12', display: "Encierro de Santo Domingo Aztacame"},
        { value: '13', display: "Metro Martin Carrera"},
        { value: '14', display: "San Bartolome Actopan"},
        { value: '15', display: "Temascalapa"},
        ]
      }

    leerBase () {
        this.baseElegida = JSON.parse(appSettings.getString('labase', '{"value": -1, "display": ""}'));
        if (this.baseElegida && this.baseElegida.value && this.baseElegida.value > 0) {
            Toast.makeText( this.baseElegida.display).show();
            this.existeBase = true;
        }
    }

    banderazo() {
        console.log('BAnderazo');
        login({
            title: "Inicio",
            message: "Para continuar ingresa la clave",
            okButtonText: "Ok",
            cancelButtonText: "Cancelar",
            password: ""
        }).then((result) => {
            // The result property is true if the dialog is closed with the OK button, false if closed with the Cancel button or undefined if closed with a neutral button.
            console.log("Dialog result: " + result.result);
            console.log("Username: " + result.userName);
            console.log("Password: " + result.password);

            if(result.password == "1234567890") {
                const options: ModalDialogOptions = {
                    viewContainerRef: this.viewContainerRef,
                    fullscreen: false,
                    context: {}
                };
                this.modalService.showModal(BanderazoComponent, options);
            }
        });
    }
}
