import { Component, OnInit } from '@angular/core';
import {ModalDialogParams} from "nativescript-angular";
import { SelectedIndexChangedEventData, ValueList } from "nativescript-drop-down";
import * as Toast from "nativescript-toast";
import { request,  HttpResponse } from "tns-core-modules/http";

@Component({
  selector: 'app-banderazo',
  templateUrl: './banderazo.component.html',
  styleUrls: ['./banderazo.component.css']
})
export class BanderazoComponent implements OnInit {
  itemsSalida = new ValueList<string>();
  itemsLlegda = new ValueList<string>();
  baseSalida: {value: string, display: string} = null;
  baseLlegada: {value: string, display: string} = null;
  guardando = false;
  mitabusApiUrl: string = "http://mitabus.com/webservice/banderazo/";
  constructor(private params: ModalDialogParams) { }

  ngOnInit() {
    this.initBases();
  }

  close() {
    this.params.closeCallback();
  }


  initBases() {
    let items = this.makeBases();
    items.forEach(i => {
      this.itemsSalida.push(i);
      this.itemsLlegda.push(i);
    });
  }

  makeBases(): {value: string, display: string}[] {
    return [
      { value: '1',  display: "Central Pachuca"},
      { value: '2',  display: "Soriana Del Valle"},
      { value: '5',  display: "Villas de Pachuca"},
      { value: '7',  display: "San Javier"},
      { value: '11', display: "Tizayuca Centro"},
      { value: '4',  display: "Tecamac"},
      { value: '6',  display: "Potrero"},
      { value: '9',  display: "La 3030"},
      { value: '3',  display: "Tizayuca Carpa"},
      // { value: '10', display: "El Carmen"},
      // { value: '8',  display: "18 de Marzo"},
      { value: '12', display: "Encierro de Santo Domingo Aztacame"},
      { value: '13', display: "Metro Martin Carrera"},
      { value: '14', display: "San Bartolome Actopan"},
      { value: '15', display: "Temascalapa"},
      ]
  }

  selectedIndexChangedSalida(args: SelectedIndexChangedEventData) {
      this.baseSalida = this.itemsSalida.getItem(args.newIndex);
  }
  selectedIndexChangedLlegada(args: SelectedIndexChangedEventData) {
    this.baseLlegada = this.itemsLlegda.getItem(args.newIndex);
  }

  doSalida() {
    if (!this.guardando) {
      if (this.baseSalida && this.baseLlegada && this.baseSalida.display && this.baseLlegada.display) {
          this.guardando = true;
          Toast.makeText( `Base Salida: ${this.baseSalida.display} -> Llegada: ${this.baseLlegada.display}`).show();
          this.enviarHttpSalida({
            idBaseSalida: this.baseSalida.value,
            idBaseLlegada:  this.baseLlegada.value,
          });

      }
    }
    
  }

  enviarHttpSalida({ idBaseSalida, idBaseLlegada }: { idBaseSalida: string; idBaseLlegada: string; lat?: number; long?: number }) {
    const urlEnviada = this.mitabusApiUrl + `${idBaseSalida}/${idBaseLlegada}/`;
    request({
        url: urlEnviada,
        method: "GET"
    }).then((response: HttpResponse) => {
        // Content property of the response is HttpContent
        // The toString method allows you to get the response body as string.
        const str = response.content.toString();
        // The toJSON method allows you to parse the received content to JSON object
        // var obj = response.content.toJSON();
        // The toImage method allows you to get the response body as ImageSource.
        // var img = response.content.toImage();
        alert(`Petición enviada ${str}` );
        this.guardando = false;
        this.close();
    }, (e) => {
    });
}



}
