(global["webpackJsonp"] = global["webpackJsonp"] || []).push([[0],{

/***/ "./banderazo/banderazo.component.css":
/***/ (function(module, exports) {

module.exports = "/* Add mobile styles for the component here.  */\r\n"

/***/ }),

/***/ "./banderazo/banderazo.component.html":
/***/ (function(module, exports) {

module.exports = "<StackLayout class=\"p-20 mt-15\">\n        <StackLayout class=\"mt-20\" orientation=\"vertical\">\n                <Label text=\"Salida\"></Label>\n                <DropDown #dd [disabled]=\"guardando\" [items]=\"itemsSalida\"\n                        style=\"padding: 10\"\n                        (selectedIndexChanged)=\"selectedIndexChangedSalida($event)\"></DropDown>\n        </StackLayout>\n\n        <StackLayout class=\"mt-20\" orientation=\"vertical\">\n                <Label text=\"Llegada\"></Label>\n                <DropDown #dd2 [items]=\"itemsLlegda\" [disabled]=\"guardando\"\n                        style=\"padding: 10\"\n                        (selectedIndexChanged)=\"selectedIndexChangedLlegada($event)\"></DropDown>\n        </StackLayout>\n\n        <StackLayout orientation=\"horizontal\">\n                <Button class=\"btn btn-primary\" text=\"Salida\" (tap)=\"doSalida()\" [disabled]=\"guardando\"></Button>\n                <Button class=\"btn btn-outline\" text=\"Cerrar\" (tap)=\"close()\" [disabled]=\"guardando\"></Button>\n        </StackLayout>\n\n\n</StackLayout>"

/***/ }),

/***/ "./banderazo/banderazo.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BanderazoComponent", function() { return BanderazoComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/index.js");
/* harmony import */ var nativescript_angular__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var nativescript_drop_down__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/nativescript-drop-down/drop-down.js");
/* harmony import */ var nativescript_drop_down__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(nativescript_drop_down__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var nativescript_toast__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/nativescript-toast/toast.js");
/* harmony import */ var nativescript_toast__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_toast__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var tns_core_modules_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/tns-core-modules/http/http.js");
/* harmony import */ var tns_core_modules_http__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_http__WEBPACK_IMPORTED_MODULE_4__);





var BanderazoComponent = /** @class */ (function () {
    function BanderazoComponent(params) {
        this.params = params;
        this.itemsSalida = new nativescript_drop_down__WEBPACK_IMPORTED_MODULE_2__["ValueList"]();
        this.itemsLlegda = new nativescript_drop_down__WEBPACK_IMPORTED_MODULE_2__["ValueList"]();
        this.baseSalida = null;
        this.baseLlegada = null;
        this.guardando = false;
        this.mitabusApiUrl = "http://mitabus.com/webservice/banderazo/";
    }
    BanderazoComponent.prototype.ngOnInit = function () {
        this.initBases();
    };
    BanderazoComponent.prototype.close = function () {
        this.params.closeCallback();
    };
    BanderazoComponent.prototype.initBases = function () {
        var _this = this;
        var items = this.makeBases();
        items.forEach(function (i) {
            _this.itemsSalida.push(i);
            _this.itemsLlegda.push(i);
        });
    };
    BanderazoComponent.prototype.makeBases = function () {
        return [
            { value: '1', display: "Central Pachuca" },
            { value: '2', display: "Soriana Del Valle" },
            { value: '5', display: "Villas de Pachuca" },
            { value: '7', display: "San Javier" },
            { value: '11', display: "Tizayuca Centro" },
            { value: '4', display: "Tecamac" },
            { value: '6', display: "Potrero" },
            { value: '9', display: "La 3030" },
            { value: '3', display: "Tizayuca Carpa" },
            // { value: '10', display: "El Carmen"},
            // { value: '8',  display: "18 de Marzo"},
            { value: '12', display: "Encierro de Santo Domingo Aztacame" },
            { value: '13', display: "Metro Martin Carrera" },
            { value: '14', display: "San Bartolome Actopan" },
            { value: '15', display: "Temascalapa" },
        ];
    };
    BanderazoComponent.prototype.selectedIndexChangedSalida = function (args) {
        this.baseSalida = this.itemsSalida.getItem(args.newIndex);
    };
    BanderazoComponent.prototype.selectedIndexChangedLlegada = function (args) {
        this.baseLlegada = this.itemsLlegda.getItem(args.newIndex);
    };
    BanderazoComponent.prototype.doSalida = function () {
        if (!this.guardando) {
            if (this.baseSalida && this.baseLlegada && this.baseSalida.display && this.baseLlegada.display) {
                this.guardando = true;
                nativescript_toast__WEBPACK_IMPORTED_MODULE_3__["makeText"]("Base Salida: " + this.baseSalida.display + " -> Llegada: " + this.baseLlegada.display).show();
                this.enviarHttpSalida({
                    idBaseSalida: this.baseSalida.value,
                    idBaseLlegada: this.baseLlegada.value,
                });
            }
        }
    };
    BanderazoComponent.prototype.enviarHttpSalida = function (_a) {
        var _this = this;
        var idBaseSalida = _a.idBaseSalida, idBaseLlegada = _a.idBaseLlegada;
        var urlEnviada = this.mitabusApiUrl + (idBaseSalida + "/" + idBaseLlegada + "/");
        Object(tns_core_modules_http__WEBPACK_IMPORTED_MODULE_4__["request"])({
            url: urlEnviada,
            method: "GET"
        }).then(function (response) {
            // Content property of the response is HttpContent
            // The toString method allows you to get the response body as string.
            var str = response.content.toString();
            // The toJSON method allows you to parse the received content to JSON object
            // var obj = response.content.toJSON();
            // The toImage method allows you to get the response body as ImageSource.
            // var img = response.content.toImage();
            alert("Petici\u00F3n enviada " + str);
            _this.guardando = false;
            _this.close();
        }, function (e) {
        });
    };
    BanderazoComponent.ctorParameters = function () { return [
        { type: nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["ModalDialogParams"] }
    ]; };
    BanderazoComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-banderazo',
            template: __webpack_require__("./banderazo/banderazo.component.html"),
            styles: [__webpack_require__("./banderazo/banderazo.component.css")]
        }),
        __metadata("design:paramtypes", [nativescript_angular__WEBPACK_IMPORTED_MODULE_1__["ModalDialogParams"]])
    ], BanderazoComponent);
    return BanderazoComponent;
}());



/***/ }),

/***/ "./home/home-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeRoutingModule", function() { return HomeRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/router/index.js");
/* harmony import */ var nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./home/home.component.ts");



var routes = [
    { path: "", component: _home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"] }
];
var HomeRoutingModule = /** @class */ (function () {
    function HomeRoutingModule() {
    }
    HomeRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"].forChild(routes)],
            exports: [nativescript_angular_router__WEBPACK_IMPORTED_MODULE_1__["NativeScriptRouterModule"]]
        })
    ], HomeRoutingModule);
    return HomeRoutingModule;
}());



/***/ }),

/***/ "./home/home.component.css":
/***/ (function(module, exports) {

module.exports = ".home-panel{\n    font-size: 16;\n    margin: 15;\n}\n\n.description-label{\n    margin-bottom: 15;\n}\n\n.btn {\n    font-size: 18;\n}\n\n.lbl {\n    margin-bottom: 20;\n}\n\n.heading {\n    font-size: 24;\n    font-weight: 600;\n}\n\n.strong {\n    font-weight: 600;\n}\n\n.input {\n    margin: 10;\n}\n\n.hr-light {\n    margin: 10;\n}\n\n.m-5 {\n    margin: 5\n}\n\n.m-15 {\n    margin: 15\n}\n\n.mr-15 {\n    margin-right: 15\n}"

/***/ }),

/***/ "./home/home.component.html":
/***/ (function(module, exports) {

module.exports = "<StackLayout sdkToggleNavButton>\n\t<ActionBar title=\"MITABus\" class=\"action-bar\" android.icon=\"res://icon\" android.iconVisibility=\"always\">\n\t\t<ActionItem text=\"Salidas\" ios.position=\"right\" android.position=\"popup\" (tap)=\"banderazo()\"></ActionItem>\n\t\t<Label text=\"V1.1.6a\" class=\"heading\"></Label>\n\t</ActionBar>\n</StackLayout>\n\n<ScrollView class=\"page\">\n\t<StackLayout class=\"home-panel form input-field\">\n\t\t<Label *ngIf=\"!existeBase\" text=\"Si no hay una base elegida por favor selecciona una\" textWrap=\"true\"\n\t\t class=\"lbl\"></Label>\n\n\t\t<!-- Base -->\n\t\t<StackLayout orientation=\"horizontal\">\n\t\t<Label text=\"Base: \" class=\"heading\"></Label>\n\t\t<Label [text]=\"baseElegida.display\" class=\"heading\" *ngIf=\"existeBase\"></Label>\n\t\t</StackLayout>\n\n\t\t<StackLayout *ngIf=\"!existeBase\">\n\t\t\t<DropDown #dd\n\t\t\t\t\t  [items]=\"items\"\n\t\t\t\t\t  [(ngModel)]=\"selectedIndex\"\n\t\t\t\t\t  (selectedIndexChanged)=\"selectedIndexChanged($event)\"\n\t\t\t\t\t  style=\"padding: 15; text-align: center\"\n\t\t\t\t\t  ></DropDown>\n\t\t\t<Button text=\"Guardar\" (tap)=\"guardarBase()\" class=\"btn btn-primary\"></Button>\n\t\t</StackLayout>\n\n\n\t\t <!-- Scan ODT -->\n\t\t<StackLayout *ngIf=\"existeBase\" style=\"padding-bottom: 20\">\n\t\t\t<Button text=\"Escanear ODT\" (tap)=\"onScan()\" class=\"btn btn-primary\"></Button>\n\t\t</StackLayout>\n\t\t\n\t\t <!-- Scan Otros -->\n\t\t<StackLayout  *ngIf=\"existeBase\">\n\t\t\t<Button text=\"Registrar Otros\" (tap)=\"displayPromptDialog()\" class=\"btn btn-secondary\"></Button>\n\t\t</StackLayout>\n\n\t\t<StackLayout class=\"hr-light\"></StackLayout>\n\t\t\n\n\n\t</StackLayout>\n</ScrollView>\n"

/***/ }),

/***/ "./home/home.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var tns_core_modules_application_settings__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/tns-core-modules/application-settings/application-settings.js");
/* harmony import */ var tns_core_modules_application_settings__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_application_settings__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/tns-core-modules/ui/dialogs/dialogs.js");
/* harmony import */ var tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/nativescript-barcodescanner/barcodescanner.js");
/* harmony import */ var nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var tns_core_modules_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/tns-core-modules/http/http.js");
/* harmony import */ var tns_core_modules_http__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(tns_core_modules_http__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var nativescript_toast__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("../node_modules/nativescript-toast/toast.js");
/* harmony import */ var nativescript_toast__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(nativescript_toast__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var nativescript_drop_down__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("../node_modules/nativescript-drop-down/drop-down.js");
/* harmony import */ var nativescript_drop_down__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(nativescript_drop_down__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var nativescript_angular_modal_dialog__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("../node_modules/nativescript-angular/modal-dialog.js");
/* harmony import */ var nativescript_angular_modal_dialog__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_modal_dialog__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _banderazo_banderazo_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__("./banderazo/banderazo.component.ts");










var HomeComponent = /** @class */ (function () {
    function HomeComponent(barcodeScanner, modalService, viewContainerRef) {
        this.barcodeScanner = barcodeScanner;
        this.modalService = modalService;
        this.viewContainerRef = viewContainerRef;
        this.num = "";
        this.str = "";
        this.bool = false;
        this.qrCodeScaned = "";
        this.mitabusApiUrlOdz = "http://mitabus.com/webservice/getcheck/";
        this.mitabusApiUrlOtros = "http://mitabus.com/webservice/getcheckother/";
        this.urlEnviada = '';
        this.items = new nativescript_drop_down__WEBPACK_IMPORTED_MODULE_6__["ValueList"]();
        this.selectedIndex = 1;
        this.baseElegida = null;
        this.existeBase = false;
    }
    HomeComponent.prototype.displayPromptDialog = function () {
        var _this = this;
        // >> prompt-dialog-code
        /*
        import {
            prompt,
            PromptResult,
            PromptOptions,
            inputType,
            capitalizationType
        } from "tns-core-modules/ui/dialogs";
        */
        var options = {
            title: "Registrar Otro",
            okButtonText: "Enviar",
            cancelButtonText: "Cancelar",
            cancelable: true,
            inputType: tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_2__["inputType"].number,
            capitalizationType: tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_2__["capitalizationType"].sentences // all. none, sentences or words
        };
        Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_2__["prompt"])(options).then(function (result) {
            if (result.text.length) {
                _this.enviarHttpOtro({ idBase: _this.baseElegida.value, numero: result.text });
            }
        });
        // << prompt-dialog-code
    };
    HomeComponent.prototype.ngOnInit = function () {
        this.initBases();
        // second parameter to getXXX() is a default value
        var num = tns_core_modules_application_settings__WEBPACK_IMPORTED_MODULE_1__["getNumber"]("someNumber", 0);
        this.num = num === 0 ? "" : num.toString();
        this.str = tns_core_modules_application_settings__WEBPACK_IMPORTED_MODULE_1__["getString"]("someString", "");
        this.bool = tns_core_modules_application_settings__WEBPACK_IMPORTED_MODULE_1__["getBoolean"]("someBoolean", false);
        this.leerBase();
    };
    HomeComponent.prototype.onScan = function () {
        var _this = this;
        var scan = function () {
            _this.barcodeScanner.scan({
                formats: "QR_CODE, EAN_13",
                beepOnScan: true,
                reportDuplicates: true,
                preferFrontCamera: false
            })
                .then(function (result) {
                if (result.text.length == 40) {
                    _this.qrCodeScaned = result.text;
                    console.log(JSON.stringify(result));
                    _this.enviarHttpOdz({ idBase: _this.baseElegida.value, qr: result.text });
                }
            })
                .catch(function (error) { return console.log(error); });
        };
        this.barcodeScanner.hasCameraPermission()
            .then(function (granted) { return granted ? scan() : console.log("Permission denied"); })
            .catch(function () {
            _this.barcodeScanner.requestCameraPermission()
                .then(function () { return scan(); });
        });
    };
    HomeComponent.prototype.enviarHttpOdz = function (_a) {
        var idBase = _a.idBase, qr = _a.qr, lat = _a.lat, long = _a.long;
        this.urlEnviada = this.mitabusApiUrlOdz + (idBase + "/" + qr + "/");
        Object(tns_core_modules_http__WEBPACK_IMPORTED_MODULE_4__["request"])({
            url: this.urlEnviada,
            method: "GET"
        }).then(function (response) {
            // Content property of the response is HttpContent
            // The toString method allows you to get the response body as string.
            var str = response.content.toString();
            // The toJSON method allows you to parse the received content to JSON object
            // var obj = response.content.toJSON();
            // The toImage method allows you to get the response body as ImageSource.
            // var img = response.content.toImage();
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_2__["alert"])("Petici\u00F3n enviada " + str);
        }, function (e) {
        });
    };
    HomeComponent.prototype.enviarHttpOtro = function (_a) {
        var idBase = _a.idBase, numero = _a.numero, lat = _a.lat, long = _a.long;
        this.urlEnviada = this.mitabusApiUrlOtros + (idBase + "/" + numero + "/");
        Object(tns_core_modules_http__WEBPACK_IMPORTED_MODULE_4__["request"])({
            url: this.urlEnviada,
            method: "GET"
        }).then(function (response) {
            // Content property of the response is HttpContent
            // The toString method allows you to get the response body as string.
            var str = response.content.toString();
            // The toJSON method allows you to parse the received content to JSON object
            // var obj = response.content.toJSON();
            // The toImage method allows you to get the response body as ImageSource.
            // var img = response.content.toImage();
            Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_2__["alert"])("Petici\u00F3n enviada " + str);
        }, function (e) {
        });
    };
    HomeComponent.prototype.selectedIndexChanged = function (args) {
        this.baseElegida = this.items.getItem(args.newIndex);
    };
    HomeComponent.prototype.guardarBase = function () {
        tns_core_modules_application_settings__WEBPACK_IMPORTED_MODULE_1__["setString"]('labase', JSON.stringify(this.baseElegida));
        nativescript_toast__WEBPACK_IMPORTED_MODULE_5__["makeText"]('base: ' + this.baseElegida.display).show();
        this.leerBase();
    };
    HomeComponent.prototype.initBases = function () {
        var _this = this;
        var items = this.makeBases();
        items.forEach(function (i) {
            _this.items.push(i);
        });
    };
    HomeComponent.prototype.makeBases = function () {
        return [
            { value: '1', display: "Central Pachuca" },
            { value: '2', display: "Soriana Del Valle" },
            { value: '5', display: "Villas de Pachuca" },
            { value: '7', display: "San Javier" },
            { value: '11', display: "Tizayuca Centro" },
            { value: '4', display: "Tecamac" },
            { value: '6', display: "Potrero" },
            { value: '9', display: "La 3030" },
            { value: '3', display: "Tizayuca Carpa" },
            // { value: '10', display: "El Carmen"},
            // { value: '8',  display: "18 de Marzo"},
            { value: '12', display: "Encierro de Santo Domingo Aztacame" },
            { value: '13', display: "Metro Martin Carrera" },
            { value: '14', display: "San Bartolome Actopan" },
            { value: '15', display: "Temascalapa" },
        ];
    };
    HomeComponent.prototype.leerBase = function () {
        this.baseElegida = JSON.parse(tns_core_modules_application_settings__WEBPACK_IMPORTED_MODULE_1__["getString"]('labase', '{"value": -1, "display": ""}'));
        if (this.baseElegida && this.baseElegida.value && this.baseElegida.value > 0) {
            nativescript_toast__WEBPACK_IMPORTED_MODULE_5__["makeText"](this.baseElegida.display).show();
            this.existeBase = true;
        }
    };
    HomeComponent.prototype.banderazo = function () {
        var _this = this;
        console.log('BAnderazo');
        Object(tns_core_modules_ui_dialogs__WEBPACK_IMPORTED_MODULE_2__["login"])({
            title: "Inicio",
            message: "Para continuar ingresa la clave",
            okButtonText: "Ok",
            cancelButtonText: "Cancelar",
            password: ""
        }).then(function (result) {
            // The result property is true if the dialog is closed with the OK button, false if closed with the Cancel button or undefined if closed with a neutral button.
            console.log("Dialog result: " + result.result);
            console.log("Username: " + result.userName);
            console.log("Password: " + result.password);
            if (result.password == "1234567890") {
                var options = {
                    viewContainerRef: _this.viewContainerRef,
                    fullscreen: false,
                    context: {}
                };
                _this.modalService.showModal(_banderazo_banderazo_component__WEBPACK_IMPORTED_MODULE_8__["BanderazoComponent"], options);
            }
        });
    };
    HomeComponent.ctorParameters = function () { return [
        { type: nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_3__["BarcodeScanner"] },
        { type: nativescript_angular_modal_dialog__WEBPACK_IMPORTED_MODULE_7__["ModalDialogService"] },
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"] }
    ]; };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: "Home",
            template: __webpack_require__("./home/home.component.html"),
            styles: [__webpack_require__("./home/home.component.css")]
        }),
        __metadata("design:paramtypes", [nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_3__["BarcodeScanner"],
            nativescript_angular_modal_dialog__WEBPACK_IMPORTED_MODULE_7__["ModalDialogService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewContainerRef"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./home/home.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("../node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("../node_modules/nativescript-angular/common.js");
/* harmony import */ var nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("../node_modules/nativescript-ui-sidedrawer/angular/side-drawer-directives.js");
/* harmony import */ var nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var nativescript_ui_listview_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("../node_modules/nativescript-ui-listview/angular/listview-directives.js");
/* harmony import */ var nativescript_ui_listview_angular__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_listview_angular__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var nativescript_ui_calendar_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("../node_modules/nativescript-ui-calendar/angular/calendar-directives.js");
/* harmony import */ var nativescript_ui_calendar_angular__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_calendar_angular__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var nativescript_ui_chart_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("../node_modules/nativescript-ui-chart/angular/chart-directives.js");
/* harmony import */ var nativescript_ui_chart_angular__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_chart_angular__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var nativescript_ui_dataform_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("../node_modules/nativescript-ui-dataform/angular/dataform-directives.js");
/* harmony import */ var nativescript_ui_dataform_angular__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_dataform_angular__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var nativescript_ui_autocomplete_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("../node_modules/nativescript-ui-autocomplete/angular/autocomplete-directives.js");
/* harmony import */ var nativescript_ui_autocomplete_angular__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_autocomplete_angular__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var nativescript_ui_gauge_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__("../node_modules/nativescript-ui-gauge/angular/gauges-directives.js");
/* harmony import */ var nativescript_ui_gauge_angular__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(nativescript_ui_gauge_angular__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__("../node_modules/nativescript-angular/forms/index.js");
/* harmony import */ var nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__("./home/home-routing.module.ts");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__("./home/home.component.ts");
/* harmony import */ var nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__("../node_modules/nativescript-barcodescanner/barcodescanner.js");
/* harmony import */ var nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var nativescript_drop_down_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__("../node_modules/nativescript-drop-down/angular/index.js");
/* harmony import */ var nativescript_drop_down_angular__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(nativescript_drop_down_angular__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var _banderazo_banderazo_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__("./banderazo/banderazo.component.ts");
/* harmony import */ var nativescript_angular_modal_dialog__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__("../node_modules/nativescript-angular/modal-dialog.js");
/* harmony import */ var nativescript_angular_modal_dialog__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(nativescript_angular_modal_dialog__WEBPACK_IMPORTED_MODULE_15__);
















var HomeModule = /** @class */ (function () {
    function HomeModule() {
    }
    HomeModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                nativescript_ui_sidedrawer_angular__WEBPACK_IMPORTED_MODULE_2__["NativeScriptUISideDrawerModule"],
                nativescript_ui_listview_angular__WEBPACK_IMPORTED_MODULE_3__["NativeScriptUIListViewModule"],
                nativescript_ui_calendar_angular__WEBPACK_IMPORTED_MODULE_4__["NativeScriptUICalendarModule"],
                nativescript_ui_chart_angular__WEBPACK_IMPORTED_MODULE_5__["NativeScriptUIChartModule"],
                nativescript_ui_dataform_angular__WEBPACK_IMPORTED_MODULE_6__["NativeScriptUIDataFormModule"],
                nativescript_ui_autocomplete_angular__WEBPACK_IMPORTED_MODULE_7__["NativeScriptUIAutoCompleteTextViewModule"],
                nativescript_ui_gauge_angular__WEBPACK_IMPORTED_MODULE_8__["NativeScriptUIGaugeModule"],
                nativescript_angular_common__WEBPACK_IMPORTED_MODULE_1__["NativeScriptCommonModule"],
                _home_routing_module__WEBPACK_IMPORTED_MODULE_10__["HomeRoutingModule"],
                nativescript_angular_forms__WEBPACK_IMPORTED_MODULE_9__["NativeScriptFormsModule"],
                nativescript_drop_down_angular__WEBPACK_IMPORTED_MODULE_13__["DropDownModule"]
            ],
            declarations: [
                _home_component__WEBPACK_IMPORTED_MODULE_11__["HomeComponent"],
                _banderazo_banderazo_component__WEBPACK_IMPORTED_MODULE_14__["BanderazoComponent"]
            ],
            schemas: [
                _angular_core__WEBPACK_IMPORTED_MODULE_0__["NO_ERRORS_SCHEMA"]
            ],
            providers: [nativescript_barcodescanner__WEBPACK_IMPORTED_MODULE_12__["BarcodeScanner"], nativescript_angular_modal_dialog__WEBPACK_IMPORTED_MODULE_15__["ModalDialogService"]],
            entryComponents: [_banderazo_banderazo_component__WEBPACK_IMPORTED_MODULE_14__["BanderazoComponent"]]
        })
    ], HomeModule);
    return HomeModule;
}());

;
    if (false) {}


/***/ })

}]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9iYW5kZXJhem8vYmFuZGVyYXpvLmNvbXBvbmVudC5jc3MiLCJ3ZWJwYWNrOi8vLy4vYmFuZGVyYXpvL2JhbmRlcmF6by5jb21wb25lbnQuaHRtbCIsIndlYnBhY2s6Ly8vLi9iYW5kZXJhem8vYmFuZGVyYXpvLmNvbXBvbmVudC50cyIsIndlYnBhY2s6Ly8vLi9ob21lL2hvbWUtcm91dGluZy5tb2R1bGUudHMiLCJ3ZWJwYWNrOi8vLy4vaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJ3ZWJwYWNrOi8vLy4vaG9tZS9ob21lLmNvbXBvbmVudC5odG1sIiwid2VicGFjazovLy8uL2hvbWUvaG9tZS5jb21wb25lbnQudHMiLCJ3ZWJwYWNrOi8vLy4vaG9tZS9ob21lLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLHVFOzs7Ozs7O0FDQUEsbW1DOzs7Ozs7OztBQ0FBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBa0Q7QUFDSztBQUMyQjtBQUN0QztBQUNtQjtBQU8vRDtJQU9FLDRCQUFvQixNQUF5QjtRQUF6QixXQUFNLEdBQU4sTUFBTSxDQUFtQjtRQU43QyxnQkFBVyxHQUFHLElBQUksZ0VBQVMsRUFBVSxDQUFDO1FBQ3RDLGdCQUFXLEdBQUcsSUFBSSxnRUFBUyxFQUFVLENBQUM7UUFDdEMsZUFBVSxHQUFxQyxJQUFJLENBQUM7UUFDcEQsZ0JBQVcsR0FBcUMsSUFBSSxDQUFDO1FBQ3JELGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDbEIsa0JBQWEsR0FBVywwQ0FBMEMsQ0FBQztJQUNsQixDQUFDO0lBRWxELHFDQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUVELGtDQUFLLEdBQUw7UUFDRSxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQzlCLENBQUM7SUFHRCxzQ0FBUyxHQUFUO1FBQUEsaUJBTUM7UUFMQyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7UUFDN0IsS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFDO1lBQ2IsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsc0NBQVMsR0FBVDtRQUNFLE9BQU87WUFDTCxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUcsT0FBTyxFQUFFLGlCQUFpQixFQUFDO1lBQzFDLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRyxPQUFPLEVBQUUsbUJBQW1CLEVBQUM7WUFDNUMsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFHLE9BQU8sRUFBRSxtQkFBbUIsRUFBQztZQUM1QyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUcsT0FBTyxFQUFFLFlBQVksRUFBQztZQUNyQyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLGlCQUFpQixFQUFDO1lBQzFDLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRyxPQUFPLEVBQUUsU0FBUyxFQUFDO1lBQ2xDLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRyxPQUFPLEVBQUUsU0FBUyxFQUFDO1lBQ2xDLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRyxPQUFPLEVBQUUsU0FBUyxFQUFDO1lBQ2xDLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUM7WUFDekMsd0NBQXdDO1lBQ3hDLDBDQUEwQztZQUMxQyxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLG9DQUFvQyxFQUFDO1lBQzdELEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsc0JBQXNCLEVBQUM7WUFDL0MsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSx1QkFBdUIsRUFBQztZQUNoRCxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLGFBQWEsRUFBQztTQUNyQztJQUNMLENBQUM7SUFFRCx1REFBMEIsR0FBMUIsVUFBMkIsSUFBbUM7UUFDMUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUNELHdEQUEyQixHQUEzQixVQUE0QixJQUFtQztRQUM3RCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUM3RCxDQUFDO0lBRUQscUNBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ25CLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFO2dCQUM1RixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztnQkFDdEIsMkRBQWMsQ0FBRSxrQkFBZ0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLHFCQUFnQixJQUFJLENBQUMsV0FBVyxDQUFDLE9BQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUMxRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7b0JBQ3BCLFlBQVksRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUs7b0JBQ25DLGFBQWEsRUFBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUs7aUJBQ3ZDLENBQUMsQ0FBQzthQUVOO1NBQ0Y7SUFFSCxDQUFDO0lBRUQsNkNBQWdCLEdBQWhCLFVBQWlCLEVBQTZHO1FBQTlILGlCQWtCRDtZQWxCb0IsOEJBQVksRUFBRSxnQ0FBYTtRQUM1QyxJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYSxJQUFNLFlBQVksU0FBSSxhQUFhLE1BQUcsRUFBQztRQUM1RSxxRUFBTyxDQUFDO1lBQ0osR0FBRyxFQUFFLFVBQVU7WUFDZixNQUFNLEVBQUUsS0FBSztTQUNoQixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsUUFBc0I7WUFDM0Isa0RBQWtEO1lBQ2xELHFFQUFxRTtZQUNyRSxJQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hDLDRFQUE0RTtZQUM1RSx1Q0FBdUM7WUFDdkMseUVBQXlFO1lBQ3pFLHdDQUF3QztZQUN4QyxLQUFLLENBQUMsMkJBQW9CLEdBQUssQ0FBRSxDQUFDO1lBQ2xDLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1lBQ3ZCLEtBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNqQixDQUFDLEVBQUUsVUFBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOztnQkEvRTZCLHNFQUFpQjs7SUFQbEMsa0JBQWtCO1FBTDlCLCtEQUFTLENBQUM7WUFDVCxRQUFRLEVBQUUsZUFBZTtZQUN6QixxRUFBeUM7O1NBRTFDLENBQUM7eUNBUTRCLHNFQUFpQjtPQVBsQyxrQkFBa0IsQ0EwRjlCO0lBQUQseUJBQUM7Q0FBQTtBQTFGOEI7Ozs7Ozs7OztBQ1gvQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBeUM7QUFFOEI7QUFFdEI7QUFFakQsSUFBTSxNQUFNLEdBQVc7SUFDbkIsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSw2REFBYSxFQUFFO0NBQ3pDLENBQUM7QUFNRjtJQUFBO0lBQWlDLENBQUM7SUFBckIsaUJBQWlCO1FBSjdCLDhEQUFRLENBQUM7WUFDTixPQUFPLEVBQUUsQ0FBQyxvRkFBd0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDcEQsT0FBTyxFQUFFLENBQUMsb0ZBQXdCLENBQUM7U0FDdEMsQ0FBQztPQUNXLGlCQUFpQixDQUFJO0lBQUQsd0JBQUM7Q0FBQTtBQUFKOzs7Ozs7OztBQ2Q5Qiw4QkFBOEIsb0JBQW9CLGlCQUFpQixHQUFHLHVCQUF1Qix3QkFBd0IsR0FBRyxVQUFVLG9CQUFvQixHQUFHLFVBQVUsd0JBQXdCLEdBQUcsY0FBYyxvQkFBb0IsdUJBQXVCLEdBQUcsYUFBYSx1QkFBdUIsR0FBRyxZQUFZLGlCQUFpQixHQUFHLGVBQWUsaUJBQWlCLEdBQUcsVUFBVSxrQkFBa0IsV0FBVyxtQkFBbUIsWUFBWSx5QkFBeUIsQzs7Ozs7OztBQ0FuYyxvakNBQW9qQywrcEI7Ozs7Ozs7O0FDQXBqQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBeUY7QUFDcEI7QUFDWjtBQUNnQjtBQUNWO0FBQ2tEO0FBRXJFO0FBQ3NDO0FBQ087QUFDdEI7QUFXbkU7SUFrQkksdUJBQW9CLGNBQThCLEVBQzlCLFlBQWdDLEVBQ2hDLGdCQUFrQztRQUZsQyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsaUJBQVksR0FBWixZQUFZLENBQW9CO1FBQ2hDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFuQnRELFFBQUcsR0FBVyxFQUFFLENBQUM7UUFDakIsUUFBRyxHQUFXLEVBQUUsQ0FBQztRQUNqQixTQUFJLEdBQVksS0FBSyxDQUFDO1FBSXRCLGlCQUFZLEdBQVcsRUFBRSxDQUFDO1FBQzFCLHFCQUFnQixHQUFXLHlDQUF5QyxDQUFDO1FBQ3JFLHVCQUFrQixHQUFXLDhDQUE4QyxDQUFDO1FBQzVFLGVBQVUsR0FBRyxFQUFFLENBQUM7UUFFaEIsVUFBSyxHQUFHLElBQUksZ0VBQVMsRUFBVSxDQUFDO1FBQ2hDLGtCQUFhLEdBQUcsQ0FBQyxDQUFDO1FBQ2xCLGdCQUFXLEdBQVEsSUFBSSxDQUFDO1FBQ3hCLGVBQVUsR0FBSSxLQUFLLENBQUM7SUFNcEIsQ0FBQztJQUVELDJDQUFtQixHQUFuQjtRQUFBLGlCQTBCQztRQXpCRyx3QkFBd0I7UUFDeEI7Ozs7Ozs7O1VBUUU7UUFDRixJQUFJLE9BQU8sR0FBa0I7WUFDekIsS0FBSyxFQUFFLGdCQUFnQjtZQUN2QixZQUFZLEVBQUUsUUFBUTtZQUN0QixnQkFBZ0IsRUFBRSxVQUFVO1lBQzVCLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLFNBQVMsRUFBRSxxRUFBUyxDQUFDLE1BQU07WUFDM0Isa0JBQWtCLEVBQUUsOEVBQWtCLENBQUMsU0FBUyxDQUFDLGdDQUFnQztTQUNwRixDQUFDO1FBRUYsMEVBQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxNQUFvQjtZQUN0QyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUNwQixLQUFJLENBQUMsY0FBYyxDQUFDLEVBQUMsTUFBTSxFQUFFLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsSUFBSSxFQUFDLENBQUM7YUFDN0U7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILHdCQUF3QjtJQUM1QixDQUFDO0lBRUQsZ0NBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUVqQixrREFBa0Q7UUFDbEQsSUFBTSxHQUFHLEdBQUcsK0VBQXFCLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDM0MsSUFBSSxDQUFDLEdBQUcsR0FBRywrRUFBcUIsQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLElBQUksR0FBRyxnRkFBc0IsQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ25CLENBQUM7SUFHRCw4QkFBTSxHQUFOO1FBQUEsaUJBeUJDO1FBdkJHLElBQUksSUFBSSxHQUFHO1lBQ1AsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZCLE9BQU8sRUFBRSxpQkFBaUI7Z0JBQzFCLFVBQVUsRUFBRSxJQUFJO2dCQUNoQixnQkFBZ0IsRUFBRSxJQUFJO2dCQUN0QixpQkFBaUIsRUFBRSxLQUFLO2FBQ3pCLENBQUM7aUJBQ0csSUFBSSxDQUFDLFVBQUMsTUFBa0I7Z0JBQ3JCLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksRUFBRSxFQUFHO29CQUMzQixLQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7b0JBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO29CQUNwQyxLQUFJLENBQUMsYUFBYSxDQUFDLEVBQUMsTUFBTSxFQUFFLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLEVBQUUsRUFBRSxNQUFNLENBQUMsSUFBSSxFQUFDLENBQUMsQ0FBQztpQkFDekU7WUFDSixDQUFDLENBQUM7aUJBQ0YsS0FBSyxDQUFDLGVBQUssSUFBSSxjQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxFQUFsQixDQUFrQixDQUFDLENBQUM7UUFDMUMsQ0FBQyxDQUFDO1FBRUYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsRUFBRTthQUN4QyxJQUFJLENBQUMsaUJBQU8sSUFBSSxjQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLEVBQW5ELENBQW1ELENBQUM7YUFDcEUsS0FBSyxDQUFDO1lBQ0wsS0FBSSxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsRUFBRTtpQkFDeEMsSUFBSSxDQUFDLGNBQU0sV0FBSSxFQUFFLEVBQU4sQ0FBTSxDQUFDLENBQUM7UUFDMUIsQ0FBQyxDQUFDLENBQUM7SUFDVCxDQUFDO0lBRUQscUNBQWEsR0FBYixVQUFjLEVBQXNGO1lBQXBGLGtCQUFNLEVBQUUsVUFBRSxFQUFFLFlBQUcsRUFBRSxjQUFJO1FBQ2pDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixJQUFNLE1BQU0sU0FBSSxFQUFFLE1BQUcsRUFBQztRQUM3RCxxRUFBTyxDQUFDO1lBQ0osR0FBRyxFQUFFLElBQUksQ0FBQyxVQUFVO1lBQ3BCLE1BQU0sRUFBRSxLQUFLO1NBQ2hCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFzQjtZQUMzQixrREFBa0Q7WUFDbEQscUVBQXFFO1lBQ3JFLElBQU0sR0FBRyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEMsNEVBQTRFO1lBQzVFLHVDQUF1QztZQUN2Qyx5RUFBeUU7WUFDekUsd0NBQXdDO1lBQ3hDLHlFQUFLLENBQUMsMkJBQW9CLEdBQUssQ0FBRSxDQUFDO1FBQ3RDLENBQUMsRUFBRSxVQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxzQ0FBYyxHQUFkLFVBQWUsRUFBK0Y7WUFBN0Ysa0JBQU0sRUFBRSxrQkFBTSxFQUFFLFlBQUcsRUFBRSxjQUFJO1FBQ3RDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixJQUFNLE1BQU0sU0FBSSxNQUFNLE1BQUcsRUFBQztRQUNuRSxxRUFBTyxDQUFDO1lBQ0osR0FBRyxFQUFFLElBQUksQ0FBQyxVQUFVO1lBQ3BCLE1BQU0sRUFBRSxLQUFLO1NBQ2hCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxRQUFzQjtZQUMzQixrREFBa0Q7WUFDbEQscUVBQXFFO1lBQ3JFLElBQU0sR0FBRyxHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEMsNEVBQTRFO1lBQzVFLHVDQUF1QztZQUN2Qyx5RUFBeUU7WUFDekUsd0NBQXdDO1lBQ3hDLHlFQUFLLENBQUMsMkJBQW9CLEdBQUssQ0FBRSxDQUFDO1FBQ3RDLENBQUMsRUFBRSxVQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw0Q0FBb0IsR0FBcEIsVUFBcUIsSUFBbUM7UUFDcEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDekQsQ0FBQztJQUVELG1DQUFXLEdBQVg7UUFDSSwrRUFBcUIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUNsRSwyREFBYyxDQUFDLFFBQVEsR0FBRSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzFELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNwQixDQUFDO0lBRUQsaUNBQVMsR0FBVDtRQUFBLGlCQUtHO1FBSkMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQzdCLEtBQUssQ0FBQyxPQUFPLENBQUMsV0FBQztZQUNiLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3JCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELGlDQUFTLEdBQVQ7UUFDRSxPQUFPO1lBQ1AsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFHLE9BQU8sRUFBRSxpQkFBaUIsRUFBQztZQUMxQyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUcsT0FBTyxFQUFFLG1CQUFtQixFQUFDO1lBQzVDLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRyxPQUFPLEVBQUUsbUJBQW1CLEVBQUM7WUFDNUMsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFHLE9BQU8sRUFBRSxZQUFZLEVBQUM7WUFDckMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBQztZQUMxQyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUcsT0FBTyxFQUFFLFNBQVMsRUFBQztZQUNsQyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUcsT0FBTyxFQUFFLFNBQVMsRUFBQztZQUNsQyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUcsT0FBTyxFQUFFLFNBQVMsRUFBQztZQUNsQyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUcsT0FBTyxFQUFFLGdCQUFnQixFQUFDO1lBQ3pDLHdDQUF3QztZQUN4QywwQ0FBMEM7WUFDMUMsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxvQ0FBb0MsRUFBQztZQUM3RCxFQUFFLEtBQUssRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLHNCQUFzQixFQUFDO1lBQy9DLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsdUJBQXVCLEVBQUM7WUFDaEQsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxhQUFhLEVBQUM7U0FDckM7SUFDSCxDQUFDO0lBRUgsZ0NBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQywrRUFBcUIsQ0FBQyxRQUFRLEVBQUUsOEJBQThCLENBQUMsQ0FBQyxDQUFDO1FBQy9GLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUU7WUFDMUUsMkRBQWMsQ0FBRSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1NBQzFCO0lBQ0wsQ0FBQztJQUVELGlDQUFTLEdBQVQ7UUFBQSxpQkF1QkM7UUF0QkcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN6Qix5RUFBSyxDQUFDO1lBQ0YsS0FBSyxFQUFFLFFBQVE7WUFDZixPQUFPLEVBQUUsaUNBQWlDO1lBQzFDLFlBQVksRUFBRSxJQUFJO1lBQ2xCLGdCQUFnQixFQUFFLFVBQVU7WUFDNUIsUUFBUSxFQUFFLEVBQUU7U0FDZixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUMsTUFBTTtZQUNYLCtKQUErSjtZQUMvSixPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMvQyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDNUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBRTVDLElBQUcsTUFBTSxDQUFDLFFBQVEsSUFBSSxZQUFZLEVBQUU7Z0JBQ2hDLElBQU0sT0FBTyxHQUF1QjtvQkFDaEMsZ0JBQWdCLEVBQUUsS0FBSSxDQUFDLGdCQUFnQjtvQkFDdkMsVUFBVSxFQUFFLEtBQUs7b0JBQ2pCLE9BQU8sRUFBRSxFQUFFO2lCQUNkLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsaUZBQWtCLEVBQUUsT0FBTyxDQUFDLENBQUM7YUFDNUQ7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7O2dCQWhMbUMsMEVBQWM7Z0JBQ2hCLG9GQUFrQjtnQkFDZCw4REFBZ0I7O0lBcEI3QyxhQUFhO1FBTnpCLCtEQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsTUFBTTtZQUVoQiwyREFBb0M7O1NBRXZDLENBQUM7eUNBbUJzQywwRUFBYztZQUNoQixvRkFBa0I7WUFDZCw4REFBZ0I7T0FwQjdDLGFBQWEsQ0FtTXpCO0lBQUQsb0JBQUM7Q0FBQTtBQW5NeUI7Ozs7Ozs7OztBQ3JCMUI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQTJEO0FBQ1k7QUFDYTtBQUNKO0FBQ0E7QUFDTjtBQUNNO0FBQ2dCO0FBQ3RCO0FBQ0w7QUFFWDtBQUNUO0FBRVk7QUFFRztBQUNHO0FBQ0U7QUEyQnJFO0lBQUE7SUFBMEIsQ0FBQztJQUFkLFVBQVU7UUF4QnRCLDhEQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsaUdBQThCO2dCQUM5Qiw2RkFBNEI7Z0JBQzVCLDZGQUE0QjtnQkFDNUIsdUZBQXlCO2dCQUN6Qiw2RkFBNEI7Z0JBQzVCLDZHQUF3QztnQkFDeEMsdUZBQXlCO2dCQUN6QixvRkFBd0I7Z0JBQ3hCLHVFQUFpQjtnQkFDakIsa0ZBQXVCO2dCQUN2Qiw4RUFBYzthQUNqQjtZQUNELFlBQVksRUFBRTtnQkFDViw4REFBYTtnQkFDYixrRkFBa0I7YUFDckI7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsOERBQWdCO2FBQ25CO1lBQ0QsU0FBUyxFQUFFLENBQUMsMkVBQWMsRUFBRSxxRkFBa0IsQ0FBQztZQUMvQyxlQUFlLEVBQUUsQ0FBRSxrRkFBa0IsQ0FBRTtTQUMxQyxDQUFDO09BQ1csVUFBVSxDQUFJO0lBQUQsaUJBQUM7Q0FBQTtBQUFKIiwiZmlsZSI6IjAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJtb2R1bGUuZXhwb3J0cyA9IFwiLyogQWRkIG1vYmlsZSBzdHlsZXMgZm9yIHRoZSBjb21wb25lbnQgaGVyZS4gICovXFxyXFxuXCIiLCJtb2R1bGUuZXhwb3J0cyA9IFwiPFN0YWNrTGF5b3V0IGNsYXNzPVxcXCJwLTIwIG10LTE1XFxcIj5cXG4gICAgICAgIDxTdGFja0xheW91dCBjbGFzcz1cXFwibXQtMjBcXFwiIG9yaWVudGF0aW9uPVxcXCJ2ZXJ0aWNhbFxcXCI+XFxuICAgICAgICAgICAgICAgIDxMYWJlbCB0ZXh0PVxcXCJTYWxpZGFcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgIDxEcm9wRG93biAjZGQgW2Rpc2FibGVkXT1cXFwiZ3VhcmRhbmRvXFxcIiBbaXRlbXNdPVxcXCJpdGVtc1NhbGlkYVxcXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT1cXFwicGFkZGluZzogMTBcXFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgKHNlbGVjdGVkSW5kZXhDaGFuZ2VkKT1cXFwic2VsZWN0ZWRJbmRleENoYW5nZWRTYWxpZGEoJGV2ZW50KVxcXCI+PC9Ecm9wRG93bj5cXG4gICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuXFxuICAgICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcIm10LTIwXFxcIiBvcmllbnRhdGlvbj1cXFwidmVydGljYWxcXFwiPlxcbiAgICAgICAgICAgICAgICA8TGFiZWwgdGV4dD1cXFwiTGxlZ2FkYVxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgPERyb3BEb3duICNkZDIgW2l0ZW1zXT1cXFwiaXRlbXNMbGVnZGFcXFwiIFtkaXNhYmxlZF09XFxcImd1YXJkYW5kb1xcXCJcXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZT1cXFwicGFkZGluZzogMTBcXFwiXFxuICAgICAgICAgICAgICAgICAgICAgICAgKHNlbGVjdGVkSW5kZXhDaGFuZ2VkKT1cXFwic2VsZWN0ZWRJbmRleENoYW5nZWRMbGVnYWRhKCRldmVudClcXFwiPjwvRHJvcERvd24+XFxuICAgICAgICA8L1N0YWNrTGF5b3V0PlxcblxcbiAgICAgICAgPFN0YWNrTGF5b3V0IG9yaWVudGF0aW9uPVxcXCJob3Jpem9udGFsXFxcIj5cXG4gICAgICAgICAgICAgICAgPEJ1dHRvbiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5XFxcIiB0ZXh0PVxcXCJTYWxpZGFcXFwiICh0YXApPVxcXCJkb1NhbGlkYSgpXFxcIiBbZGlzYWJsZWRdPVxcXCJndWFyZGFuZG9cXFwiPjwvQnV0dG9uPlxcbiAgICAgICAgICAgICAgICA8QnV0dG9uIGNsYXNzPVxcXCJidG4gYnRuLW91dGxpbmVcXFwiIHRleHQ9XFxcIkNlcnJhclxcXCIgKHRhcCk9XFxcImNsb3NlKClcXFwiIFtkaXNhYmxlZF09XFxcImd1YXJkYW5kb1xcXCI+PC9CdXR0b24+XFxuICAgICAgICA8L1N0YWNrTGF5b3V0PlxcblxcblxcbjwvU3RhY2tMYXlvdXQ+XCIiLCJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHtNb2RhbERpYWxvZ1BhcmFtc30gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyXCI7XG5pbXBvcnQgeyBTZWxlY3RlZEluZGV4Q2hhbmdlZEV2ZW50RGF0YSwgVmFsdWVMaXN0IH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1kcm9wLWRvd25cIjtcbmltcG9ydCAqIGFzIFRvYXN0IGZyb20gXCJuYXRpdmVzY3JpcHQtdG9hc3RcIjtcbmltcG9ydCB7IHJlcXVlc3QsICBIdHRwUmVzcG9uc2UgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy9odHRwXCI7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1iYW5kZXJhem8nLFxuICB0ZW1wbGF0ZVVybDogJy4vYmFuZGVyYXpvLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vYmFuZGVyYXpvLmNvbXBvbmVudC5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBCYW5kZXJhem9Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBpdGVtc1NhbGlkYSA9IG5ldyBWYWx1ZUxpc3Q8c3RyaW5nPigpO1xuICBpdGVtc0xsZWdkYSA9IG5ldyBWYWx1ZUxpc3Q8c3RyaW5nPigpO1xuICBiYXNlU2FsaWRhOiB7dmFsdWU6IHN0cmluZywgZGlzcGxheTogc3RyaW5nfSA9IG51bGw7XG4gIGJhc2VMbGVnYWRhOiB7dmFsdWU6IHN0cmluZywgZGlzcGxheTogc3RyaW5nfSA9IG51bGw7XG4gIGd1YXJkYW5kbyA9IGZhbHNlO1xuICBtaXRhYnVzQXBpVXJsOiBzdHJpbmcgPSBcImh0dHA6Ly9taXRhYnVzLmNvbS93ZWJzZXJ2aWNlL2JhbmRlcmF6by9cIjtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBwYXJhbXM6IE1vZGFsRGlhbG9nUGFyYW1zKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICB0aGlzLmluaXRCYXNlcygpO1xuICB9XG5cbiAgY2xvc2UoKSB7XG4gICAgdGhpcy5wYXJhbXMuY2xvc2VDYWxsYmFjaygpO1xuICB9XG5cblxuICBpbml0QmFzZXMoKSB7XG4gICAgbGV0IGl0ZW1zID0gdGhpcy5tYWtlQmFzZXMoKTtcbiAgICBpdGVtcy5mb3JFYWNoKGkgPT4ge1xuICAgICAgdGhpcy5pdGVtc1NhbGlkYS5wdXNoKGkpO1xuICAgICAgdGhpcy5pdGVtc0xsZWdkYS5wdXNoKGkpO1xuICAgIH0pO1xuICB9XG5cbiAgbWFrZUJhc2VzKCk6IHt2YWx1ZTogc3RyaW5nLCBkaXNwbGF5OiBzdHJpbmd9W10ge1xuICAgIHJldHVybiBbXG4gICAgICB7IHZhbHVlOiAnMScsICBkaXNwbGF5OiBcIkNlbnRyYWwgUGFjaHVjYVwifSxcbiAgICAgIHsgdmFsdWU6ICcyJywgIGRpc3BsYXk6IFwiU29yaWFuYSBEZWwgVmFsbGVcIn0sXG4gICAgICB7IHZhbHVlOiAnNScsICBkaXNwbGF5OiBcIlZpbGxhcyBkZSBQYWNodWNhXCJ9LFxuICAgICAgeyB2YWx1ZTogJzcnLCAgZGlzcGxheTogXCJTYW4gSmF2aWVyXCJ9LFxuICAgICAgeyB2YWx1ZTogJzExJywgZGlzcGxheTogXCJUaXpheXVjYSBDZW50cm9cIn0sXG4gICAgICB7IHZhbHVlOiAnNCcsICBkaXNwbGF5OiBcIlRlY2FtYWNcIn0sXG4gICAgICB7IHZhbHVlOiAnNicsICBkaXNwbGF5OiBcIlBvdHJlcm9cIn0sXG4gICAgICB7IHZhbHVlOiAnOScsICBkaXNwbGF5OiBcIkxhIDMwMzBcIn0sXG4gICAgICB7IHZhbHVlOiAnMycsICBkaXNwbGF5OiBcIlRpemF5dWNhIENhcnBhXCJ9LFxuICAgICAgLy8geyB2YWx1ZTogJzEwJywgZGlzcGxheTogXCJFbCBDYXJtZW5cIn0sXG4gICAgICAvLyB7IHZhbHVlOiAnOCcsICBkaXNwbGF5OiBcIjE4IGRlIE1hcnpvXCJ9LFxuICAgICAgeyB2YWx1ZTogJzEyJywgZGlzcGxheTogXCJFbmNpZXJybyBkZSBTYW50byBEb21pbmdvIEF6dGFjYW1lXCJ9LFxuICAgICAgeyB2YWx1ZTogJzEzJywgZGlzcGxheTogXCJNZXRybyBNYXJ0aW4gQ2FycmVyYVwifSxcbiAgICAgIHsgdmFsdWU6ICcxNCcsIGRpc3BsYXk6IFwiU2FuIEJhcnRvbG9tZSBBY3RvcGFuXCJ9LFxuICAgICAgeyB2YWx1ZTogJzE1JywgZGlzcGxheTogXCJUZW1hc2NhbGFwYVwifSxcbiAgICAgIF1cbiAgfVxuXG4gIHNlbGVjdGVkSW5kZXhDaGFuZ2VkU2FsaWRhKGFyZ3M6IFNlbGVjdGVkSW5kZXhDaGFuZ2VkRXZlbnREYXRhKSB7XG4gICAgICB0aGlzLmJhc2VTYWxpZGEgPSB0aGlzLml0ZW1zU2FsaWRhLmdldEl0ZW0oYXJncy5uZXdJbmRleCk7XG4gIH1cbiAgc2VsZWN0ZWRJbmRleENoYW5nZWRMbGVnYWRhKGFyZ3M6IFNlbGVjdGVkSW5kZXhDaGFuZ2VkRXZlbnREYXRhKSB7XG4gICAgdGhpcy5iYXNlTGxlZ2FkYSA9IHRoaXMuaXRlbXNMbGVnZGEuZ2V0SXRlbShhcmdzLm5ld0luZGV4KTtcbiAgfVxuXG4gIGRvU2FsaWRhKCkge1xuICAgIGlmICghdGhpcy5ndWFyZGFuZG8pIHtcbiAgICAgIGlmICh0aGlzLmJhc2VTYWxpZGEgJiYgdGhpcy5iYXNlTGxlZ2FkYSAmJiB0aGlzLmJhc2VTYWxpZGEuZGlzcGxheSAmJiB0aGlzLmJhc2VMbGVnYWRhLmRpc3BsYXkpIHtcbiAgICAgICAgICB0aGlzLmd1YXJkYW5kbyA9IHRydWU7XG4gICAgICAgICAgVG9hc3QubWFrZVRleHQoIGBCYXNlIFNhbGlkYTogJHt0aGlzLmJhc2VTYWxpZGEuZGlzcGxheX0gLT4gTGxlZ2FkYTogJHt0aGlzLmJhc2VMbGVnYWRhLmRpc3BsYXl9YCkuc2hvdygpO1xuICAgICAgICAgIHRoaXMuZW52aWFySHR0cFNhbGlkYSh7XG4gICAgICAgICAgICBpZEJhc2VTYWxpZGE6IHRoaXMuYmFzZVNhbGlkYS52YWx1ZSxcbiAgICAgICAgICAgIGlkQmFzZUxsZWdhZGE6ICB0aGlzLmJhc2VMbGVnYWRhLnZhbHVlLFxuICAgICAgICAgIH0pO1xuXG4gICAgICB9XG4gICAgfVxuICAgIFxuICB9XG5cbiAgZW52aWFySHR0cFNhbGlkYSh7IGlkQmFzZVNhbGlkYSwgaWRCYXNlTGxlZ2FkYSB9OiB7IGlkQmFzZVNhbGlkYTogc3RyaW5nOyBpZEJhc2VMbGVnYWRhOiBzdHJpbmc7IGxhdD86IG51bWJlcjsgbG9uZz86IG51bWJlciB9KSB7XG4gICAgY29uc3QgdXJsRW52aWFkYSA9IHRoaXMubWl0YWJ1c0FwaVVybCArIGAke2lkQmFzZVNhbGlkYX0vJHtpZEJhc2VMbGVnYWRhfS9gO1xuICAgIHJlcXVlc3Qoe1xuICAgICAgICB1cmw6IHVybEVudmlhZGEsXG4gICAgICAgIG1ldGhvZDogXCJHRVRcIlxuICAgIH0pLnRoZW4oKHJlc3BvbnNlOiBIdHRwUmVzcG9uc2UpID0+IHtcbiAgICAgICAgLy8gQ29udGVudCBwcm9wZXJ0eSBvZiB0aGUgcmVzcG9uc2UgaXMgSHR0cENvbnRlbnRcbiAgICAgICAgLy8gVGhlIHRvU3RyaW5nIG1ldGhvZCBhbGxvd3MgeW91IHRvIGdldCB0aGUgcmVzcG9uc2UgYm9keSBhcyBzdHJpbmcuXG4gICAgICAgIGNvbnN0IHN0ciA9IHJlc3BvbnNlLmNvbnRlbnQudG9TdHJpbmcoKTtcbiAgICAgICAgLy8gVGhlIHRvSlNPTiBtZXRob2QgYWxsb3dzIHlvdSB0byBwYXJzZSB0aGUgcmVjZWl2ZWQgY29udGVudCB0byBKU09OIG9iamVjdFxuICAgICAgICAvLyB2YXIgb2JqID0gcmVzcG9uc2UuY29udGVudC50b0pTT04oKTtcbiAgICAgICAgLy8gVGhlIHRvSW1hZ2UgbWV0aG9kIGFsbG93cyB5b3UgdG8gZ2V0IHRoZSByZXNwb25zZSBib2R5IGFzIEltYWdlU291cmNlLlxuICAgICAgICAvLyB2YXIgaW1nID0gcmVzcG9uc2UuY29udGVudC50b0ltYWdlKCk7XG4gICAgICAgIGFsZXJ0KGBQZXRpY2nDs24gZW52aWFkYSAke3N0cn1gICk7XG4gICAgICAgIHRoaXMuZ3VhcmRhbmRvID0gZmFsc2U7XG4gICAgICAgIHRoaXMuY2xvc2UoKTtcbiAgICB9LCAoZSkgPT4ge1xuICAgIH0pO1xufVxuXG5cblxufVxuIiwiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgUm91dGVzIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuXG5pbXBvcnQgeyBIb21lQ29tcG9uZW50IH0gZnJvbSBcIi4vaG9tZS5jb21wb25lbnRcIjtcblxuY29uc3Qgcm91dGVzOiBSb3V0ZXMgPSBbXG4gICAgeyBwYXRoOiBcIlwiLCBjb21wb25lbnQ6IEhvbWVDb21wb25lbnQgfVxuXTtcblxuQE5nTW9kdWxlKHtcbiAgICBpbXBvcnRzOiBbTmF0aXZlU2NyaXB0Um91dGVyTW9kdWxlLmZvckNoaWxkKHJvdXRlcyldLFxuICAgIGV4cG9ydHM6IFtOYXRpdmVTY3JpcHRSb3V0ZXJNb2R1bGVdXG59KVxuZXhwb3J0IGNsYXNzIEhvbWVSb3V0aW5nTW9kdWxlIHsgfVxuIiwibW9kdWxlLmV4cG9ydHMgPSBcIi5ob21lLXBhbmVse1xcbiAgICBmb250LXNpemU6IDE2O1xcbiAgICBtYXJnaW46IDE1O1xcbn1cXG5cXG4uZGVzY3JpcHRpb24tbGFiZWx7XFxuICAgIG1hcmdpbi1ib3R0b206IDE1O1xcbn1cXG5cXG4uYnRuIHtcXG4gICAgZm9udC1zaXplOiAxODtcXG59XFxuXFxuLmxibCB7XFxuICAgIG1hcmdpbi1ib3R0b206IDIwO1xcbn1cXG5cXG4uaGVhZGluZyB7XFxuICAgIGZvbnQtc2l6ZTogMjQ7XFxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XFxufVxcblxcbi5zdHJvbmcge1xcbiAgICBmb250LXdlaWdodDogNjAwO1xcbn1cXG5cXG4uaW5wdXQge1xcbiAgICBtYXJnaW46IDEwO1xcbn1cXG5cXG4uaHItbGlnaHQge1xcbiAgICBtYXJnaW46IDEwO1xcbn1cXG5cXG4ubS01IHtcXG4gICAgbWFyZ2luOiA1XFxufVxcblxcbi5tLTE1IHtcXG4gICAgbWFyZ2luOiAxNVxcbn1cXG5cXG4ubXItMTUge1xcbiAgICBtYXJnaW4tcmlnaHQ6IDE1XFxufVwiIiwibW9kdWxlLmV4cG9ydHMgPSBcIjxTdGFja0xheW91dCBzZGtUb2dnbGVOYXZCdXR0b24+XFxuXFx0PEFjdGlvbkJhciB0aXRsZT1cXFwiTUlUQUJ1c1xcXCIgY2xhc3M9XFxcImFjdGlvbi1iYXJcXFwiIGFuZHJvaWQuaWNvbj1cXFwicmVzOi8vaWNvblxcXCIgYW5kcm9pZC5pY29uVmlzaWJpbGl0eT1cXFwiYWx3YXlzXFxcIj5cXG5cXHRcXHQ8QWN0aW9uSXRlbSB0ZXh0PVxcXCJTYWxpZGFzXFxcIiBpb3MucG9zaXRpb249XFxcInJpZ2h0XFxcIiBhbmRyb2lkLnBvc2l0aW9uPVxcXCJwb3B1cFxcXCIgKHRhcCk9XFxcImJhbmRlcmF6bygpXFxcIj48L0FjdGlvbkl0ZW0+XFxuXFx0XFx0PExhYmVsIHRleHQ9XFxcIlYxLjEuNmFcXFwiIGNsYXNzPVxcXCJoZWFkaW5nXFxcIj48L0xhYmVsPlxcblxcdDwvQWN0aW9uQmFyPlxcbjwvU3RhY2tMYXlvdXQ+XFxuXFxuPFNjcm9sbFZpZXcgY2xhc3M9XFxcInBhZ2VcXFwiPlxcblxcdDxTdGFja0xheW91dCBjbGFzcz1cXFwiaG9tZS1wYW5lbCBmb3JtIGlucHV0LWZpZWxkXFxcIj5cXG5cXHRcXHQ8TGFiZWwgKm5nSWY9XFxcIiFleGlzdGVCYXNlXFxcIiB0ZXh0PVxcXCJTaSBubyBoYXkgdW5hIGJhc2UgZWxlZ2lkYSBwb3IgZmF2b3Igc2VsZWNjaW9uYSB1bmFcXFwiIHRleHRXcmFwPVxcXCJ0cnVlXFxcIlxcblxcdFxcdCBjbGFzcz1cXFwibGJsXFxcIj48L0xhYmVsPlxcblxcblxcdFxcdDwhLS0gQmFzZSAtLT5cXG5cXHRcXHQ8U3RhY2tMYXlvdXQgb3JpZW50YXRpb249XFxcImhvcml6b250YWxcXFwiPlxcblxcdFxcdDxMYWJlbCB0ZXh0PVxcXCJCYXNlOiBcXFwiIGNsYXNzPVxcXCJoZWFkaW5nXFxcIj48L0xhYmVsPlxcblxcdFxcdDxMYWJlbCBbdGV4dF09XFxcImJhc2VFbGVnaWRhLmRpc3BsYXlcXFwiIGNsYXNzPVxcXCJoZWFkaW5nXFxcIiAqbmdJZj1cXFwiZXhpc3RlQmFzZVxcXCI+PC9MYWJlbD5cXG5cXHRcXHQ8L1N0YWNrTGF5b3V0PlxcblxcblxcdFxcdDxTdGFja0xheW91dCAqbmdJZj1cXFwiIWV4aXN0ZUJhc2VcXFwiPlxcblxcdFxcdFxcdDxEcm9wRG93biAjZGRcXG5cXHRcXHRcXHRcXHRcXHQgIFtpdGVtc109XFxcIml0ZW1zXFxcIlxcblxcdFxcdFxcdFxcdFxcdCAgWyhuZ01vZGVsKV09XFxcInNlbGVjdGVkSW5kZXhcXFwiXFxuXFx0XFx0XFx0XFx0XFx0ICAoc2VsZWN0ZWRJbmRleENoYW5nZWQpPVxcXCJzZWxlY3RlZEluZGV4Q2hhbmdlZCgkZXZlbnQpXFxcIlxcblxcdFxcdFxcdFxcdFxcdCAgc3R5bGU9XFxcInBhZGRpbmc6IDE1OyB0ZXh0LWFsaWduOiBjZW50ZXJcXFwiXFxuXFx0XFx0XFx0XFx0XFx0ICA+PC9Ecm9wRG93bj5cXG5cXHRcXHRcXHQ8QnV0dG9uIHRleHQ9XFxcIkd1YXJkYXJcXFwiICh0YXApPVxcXCJndWFyZGFyQmFzZSgpXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1wcmltYXJ5XFxcIj48L0J1dHRvbj5cXG5cXHRcXHQ8L1N0YWNrTGF5b3V0PlxcblxcblxcblxcdFxcdCA8IS0tIFNjYW4gT0RUIC0tPlxcblxcdFxcdDxTdGFja0xheW91dCAqbmdJZj1cXFwiZXhpc3RlQmFzZVxcXCIgc3R5bGU9XFxcInBhZGRpbmctYm90dG9tOiAyMFxcXCI+XFxuXFx0XFx0XFx0PEJ1dHRvbiB0ZXh0PVxcXCJFc2NhbmVhciBPRFRcXFwiICh0YXApPVxcXCJvblNjYW4oKVxcXCIgY2xhc3M9XFxcImJ0biBidG4tcHJpbWFyeVxcXCI+PC9CdXR0b24+XFxuXFx0XFx0PC9TdGFja0xheW91dD5cXG5cXHRcXHRcXG5cXHRcXHQgPCEtLSBTY2FuIE90cm9zIC0tPlxcblxcdFxcdDxTdGFja0xheW91dCAgKm5nSWY9XFxcImV4aXN0ZUJhc2VcXFwiPlxcblxcdFxcdFxcdDxCdXR0b24gdGV4dD1cXFwiUmVnaXN0cmFyIE90cm9zXFxcIiAodGFwKT1cXFwiZGlzcGxheVByb21wdERpYWxvZygpXFxcIiBjbGFzcz1cXFwiYnRuIGJ0bi1zZWNvbmRhcnlcXFwiPjwvQnV0dG9uPlxcblxcdFxcdDwvU3RhY2tMYXlvdXQ+XFxuXFxuXFx0XFx0PFN0YWNrTGF5b3V0IGNsYXNzPVxcXCJoci1saWdodFxcXCI+PC9TdGFja0xheW91dD5cXG5cXHRcXHRcXG5cXG5cXG5cXHQ8L1N0YWNrTGF5b3V0PlxcbjwvU2Nyb2xsVmlldz5cXG5cIiIsImltcG9ydCB7Q29tcG9uZW50LCBFbGVtZW50UmVmLCBPbkluaXQsIFZpZXdDaGlsZCwgVmlld0NvbnRhaW5lclJlZn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCAqIGFzIGFwcFNldHRpbmdzIGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uLXNldHRpbmdzXCI7XG5pbXBvcnQge2FsZXJ0LCBsb2dpbn0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvdWkvZGlhbG9nc1wiO1xuaW1wb3J0IHsgQmFyY29kZVNjYW5uZXIsIFNjYW5SZXN1bHQgfSBmcm9tICduYXRpdmVzY3JpcHQtYmFyY29kZXNjYW5uZXInO1xuaW1wb3J0IHsgcmVxdWVzdCwgIEh0dHBSZXNwb25zZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL2h0dHBcIjtcbmltcG9ydCB7IHByb21wdCwgUHJvbXB0UmVzdWx0LCBQcm9tcHRPcHRpb25zLCBpbnB1dFR5cGUsIGNhcGl0YWxpemF0aW9uVHlwZSB9IGZyb20gXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2RpYWxvZ3NcIjtcblxuaW1wb3J0ICogYXMgVG9hc3QgZnJvbSBcIm5hdGl2ZXNjcmlwdC10b2FzdFwiO1xuaW1wb3J0IHsgU2VsZWN0ZWRJbmRleENoYW5nZWRFdmVudERhdGEsIFZhbHVlTGlzdCB9IGZyb20gXCJuYXRpdmVzY3JpcHQtZHJvcC1kb3duXCI7XG5pbXBvcnQge01vZGFsRGlhbG9nU2VydmljZSwgTW9kYWxEaWFsb2dPcHRpb25zfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvbW9kYWwtZGlhbG9nXCI7XG5pbXBvcnQge0JhbmRlcmF6b0NvbXBvbmVudH0gZnJvbSBcIn4vYmFuZGVyYXpvL2JhbmRlcmF6by5jb21wb25lbnRcIjtcblxuXG5cblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwiSG9tZVwiLFxuICAgIG1vZHVsZUlkOiBtb2R1bGUuaWQsXG4gICAgdGVtcGxhdGVVcmw6IFwiLi9ob21lLmNvbXBvbmVudC5odG1sXCIsXG4gICAgc3R5bGVVcmxzOiBbJy4vaG9tZS5jb21wb25lbnQuY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgSG9tZUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgbnVtOiBzdHJpbmcgPSBcIlwiO1xuICAgIHN0cjogc3RyaW5nID0gXCJcIjtcbiAgICBib29sOiBib29sZWFuID0gZmFsc2U7XG5cblxuXG4gICAgcXJDb2RlU2NhbmVkOiBzdHJpbmcgPSBcIlwiO1xuICAgIG1pdGFidXNBcGlVcmxPZHo6IHN0cmluZyA9IFwiaHR0cDovL21pdGFidXMuY29tL3dlYnNlcnZpY2UvZ2V0Y2hlY2svXCI7XG4gICAgbWl0YWJ1c0FwaVVybE90cm9zOiBzdHJpbmcgPSBcImh0dHA6Ly9taXRhYnVzLmNvbS93ZWJzZXJ2aWNlL2dldGNoZWNrb3RoZXIvXCI7XG4gICAgdXJsRW52aWFkYSA9ICcnO1xuXG4gICAgaXRlbXMgPSBuZXcgVmFsdWVMaXN0PHN0cmluZz4oKTtcbiAgICBzZWxlY3RlZEluZGV4ID0gMTtcbiAgICBiYXNlRWxlZ2lkYTogYW55ID0gbnVsbDtcbiAgICBleGlzdGVCYXNlICA9IGZhbHNlO1xuICAgIFxuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBiYXJjb2RlU2Nhbm5lcjogQmFyY29kZVNjYW5uZXIsXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBtb2RhbFNlcnZpY2U6IE1vZGFsRGlhbG9nU2VydmljZSxcbiAgICAgICAgICAgICAgICBwcml2YXRlIHZpZXdDb250YWluZXJSZWY6IFZpZXdDb250YWluZXJSZWYpIHtcbiAgICB9XG5cbiAgICBkaXNwbGF5UHJvbXB0RGlhbG9nKCkge1xuICAgICAgICAvLyA+PiBwcm9tcHQtZGlhbG9nLWNvZGVcbiAgICAgICAgLypcbiAgICAgICAgaW1wb3J0IHtcbiAgICAgICAgICAgIHByb21wdCxcbiAgICAgICAgICAgIFByb21wdFJlc3VsdCxcbiAgICAgICAgICAgIFByb21wdE9wdGlvbnMsXG4gICAgICAgICAgICBpbnB1dFR5cGUsXG4gICAgICAgICAgICBjYXBpdGFsaXphdGlvblR5cGVcbiAgICAgICAgfSBmcm9tIFwidG5zLWNvcmUtbW9kdWxlcy91aS9kaWFsb2dzXCI7XG4gICAgICAgICovXG4gICAgICAgIGxldCBvcHRpb25zOiBQcm9tcHRPcHRpb25zID0ge1xuICAgICAgICAgICAgdGl0bGU6IFwiUmVnaXN0cmFyIE90cm9cIixcbiAgICAgICAgICAgIG9rQnV0dG9uVGV4dDogXCJFbnZpYXJcIixcbiAgICAgICAgICAgIGNhbmNlbEJ1dHRvblRleHQ6IFwiQ2FuY2VsYXJcIixcbiAgICAgICAgICAgIGNhbmNlbGFibGU6IHRydWUsXG4gICAgICAgICAgICBpbnB1dFR5cGU6IGlucHV0VHlwZS5udW1iZXIsIC8vIGVtYWlsLCBudW1iZXIsIHRleHQsIHBhc3N3b3JkLCBvciBlbWFpbFxuICAgICAgICAgICAgY2FwaXRhbGl6YXRpb25UeXBlOiBjYXBpdGFsaXphdGlvblR5cGUuc2VudGVuY2VzIC8vIGFsbC4gbm9uZSwgc2VudGVuY2VzIG9yIHdvcmRzXG4gICAgICAgIH07XG5cbiAgICAgICAgcHJvbXB0KG9wdGlvbnMpLnRoZW4oKHJlc3VsdDogUHJvbXB0UmVzdWx0KSA9PiB7XG4gICAgICAgICAgICBpZiAocmVzdWx0LnRleHQubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5lbnZpYXJIdHRwT3Rybyh7aWRCYXNlOiB0aGlzLmJhc2VFbGVnaWRhLnZhbHVlLCBudW1lcm86IHJlc3VsdC50ZXh0fSlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIC8vIDw8IHByb21wdC1kaWFsb2ctY29kZVxuICAgIH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgICB0aGlzLmluaXRCYXNlcygpO1xuICAgICAgICBcbiAgICAgICAgLy8gc2Vjb25kIHBhcmFtZXRlciB0byBnZXRYWFgoKSBpcyBhIGRlZmF1bHQgdmFsdWVcbiAgICAgICAgY29uc3QgbnVtID0gYXBwU2V0dGluZ3MuZ2V0TnVtYmVyKFwic29tZU51bWJlclwiLCAwKTtcbiAgICAgICAgdGhpcy5udW0gPSBudW0gPT09IDAgPyBcIlwiIDogbnVtLnRvU3RyaW5nKCk7XG4gICAgICAgIHRoaXMuc3RyID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwic29tZVN0cmluZ1wiLCBcIlwiKTtcbiAgICAgICAgdGhpcy5ib29sID0gYXBwU2V0dGluZ3MuZ2V0Qm9vbGVhbihcInNvbWVCb29sZWFuXCIsIGZhbHNlKTtcbiAgICAgICB0aGlzLmxlZXJCYXNlKCk7XG4gICAgfVxuXG5cbiAgICBvblNjYW4oKSB7XG5cbiAgICAgICAgbGV0IHNjYW4gPSAoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmJhcmNvZGVTY2FubmVyLnNjYW4oe1xuICAgICAgICAgICAgICBmb3JtYXRzOiBcIlFSX0NPREUsIEVBTl8xM1wiLFxuICAgICAgICAgICAgICBiZWVwT25TY2FuOiB0cnVlLFxuICAgICAgICAgICAgICByZXBvcnREdXBsaWNhdGVzOiB0cnVlLFxuICAgICAgICAgICAgICBwcmVmZXJGcm9udENhbWVyYTogZmFsc2VcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLnRoZW4oKHJlc3VsdDogU2NhblJlc3VsdCkgPT4geyBcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdC50ZXh0Lmxlbmd0aCA9PSA0MCkgIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucXJDb2RlU2NhbmVkID0gcmVzdWx0LnRleHQ7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhKU09OLnN0cmluZ2lmeShyZXN1bHQpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZW52aWFySHR0cE9keih7aWRCYXNlOiB0aGlzLmJhc2VFbGVnaWRhLnZhbHVlLCBxcjogcmVzdWx0LnRleHR9KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5jYXRjaChlcnJvciA9PiBjb25zb2xlLmxvZyhlcnJvcikpO1xuICAgICAgICAgIH07XG5cbiAgICAgICAgICB0aGlzLmJhcmNvZGVTY2FubmVyLmhhc0NhbWVyYVBlcm1pc3Npb24oKVxuICAgICAgICAgIC50aGVuKGdyYW50ZWQgPT4gZ3JhbnRlZCA/IHNjYW4oKSA6IGNvbnNvbGUubG9nKFwiUGVybWlzc2lvbiBkZW5pZWRcIikpXG4gICAgICAgICAgLmNhdGNoKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuYmFyY29kZVNjYW5uZXIucmVxdWVzdENhbWVyYVBlcm1pc3Npb24oKVxuICAgICAgICAgICAgICAgIC50aGVuKCgpID0+IHNjYW4oKSk7XG4gICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZW52aWFySHR0cE9keih7IGlkQmFzZSwgcXIsIGxhdCwgbG9uZyB9OiB7IGlkQmFzZTogbnVtYmVyOyBxcjogc3RyaW5nOyBsYXQ/OiBudW1iZXI7IGxvbmc/OiBudW1iZXIgfSkge1xuICAgICAgICB0aGlzLnVybEVudmlhZGEgPSB0aGlzLm1pdGFidXNBcGlVcmxPZHogKyBgJHtpZEJhc2V9LyR7cXJ9L2A7XG4gICAgICAgIHJlcXVlc3Qoe1xuICAgICAgICAgICAgdXJsOiB0aGlzLnVybEVudmlhZGEsXG4gICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCJcbiAgICAgICAgfSkudGhlbigocmVzcG9uc2U6IEh0dHBSZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgLy8gQ29udGVudCBwcm9wZXJ0eSBvZiB0aGUgcmVzcG9uc2UgaXMgSHR0cENvbnRlbnRcbiAgICAgICAgICAgIC8vIFRoZSB0b1N0cmluZyBtZXRob2QgYWxsb3dzIHlvdSB0byBnZXQgdGhlIHJlc3BvbnNlIGJvZHkgYXMgc3RyaW5nLlxuICAgICAgICAgICAgY29uc3Qgc3RyID0gcmVzcG9uc2UuY29udGVudC50b1N0cmluZygpO1xuICAgICAgICAgICAgLy8gVGhlIHRvSlNPTiBtZXRob2QgYWxsb3dzIHlvdSB0byBwYXJzZSB0aGUgcmVjZWl2ZWQgY29udGVudCB0byBKU09OIG9iamVjdFxuICAgICAgICAgICAgLy8gdmFyIG9iaiA9IHJlc3BvbnNlLmNvbnRlbnQudG9KU09OKCk7XG4gICAgICAgICAgICAvLyBUaGUgdG9JbWFnZSBtZXRob2QgYWxsb3dzIHlvdSB0byBnZXQgdGhlIHJlc3BvbnNlIGJvZHkgYXMgSW1hZ2VTb3VyY2UuXG4gICAgICAgICAgICAvLyB2YXIgaW1nID0gcmVzcG9uc2UuY29udGVudC50b0ltYWdlKCk7XG4gICAgICAgICAgICBhbGVydChgUGV0aWNpw7NuIGVudmlhZGEgJHtzdHJ9YCApO1xuICAgICAgICB9LCAoZSkgPT4ge1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBlbnZpYXJIdHRwT3Rybyh7IGlkQmFzZSwgbnVtZXJvLCBsYXQsIGxvbmcgfTogeyBpZEJhc2U6IG51bWJlcjsgbnVtZXJvOiBzdHJpbmc7ICBsYXQ/OiBudW1iZXI7IGxvbmc/OiBudW1iZXIgfSkge1xuICAgICAgICB0aGlzLnVybEVudmlhZGEgPSB0aGlzLm1pdGFidXNBcGlVcmxPdHJvcyArIGAke2lkQmFzZX0vJHtudW1lcm99L2A7XG4gICAgICAgIHJlcXVlc3Qoe1xuICAgICAgICAgICAgdXJsOiB0aGlzLnVybEVudmlhZGEsXG4gICAgICAgICAgICBtZXRob2Q6IFwiR0VUXCJcbiAgICAgICAgfSkudGhlbigocmVzcG9uc2U6IEh0dHBSZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgLy8gQ29udGVudCBwcm9wZXJ0eSBvZiB0aGUgcmVzcG9uc2UgaXMgSHR0cENvbnRlbnRcbiAgICAgICAgICAgIC8vIFRoZSB0b1N0cmluZyBtZXRob2QgYWxsb3dzIHlvdSB0byBnZXQgdGhlIHJlc3BvbnNlIGJvZHkgYXMgc3RyaW5nLlxuICAgICAgICAgICAgY29uc3Qgc3RyID0gcmVzcG9uc2UuY29udGVudC50b1N0cmluZygpO1xuICAgICAgICAgICAgLy8gVGhlIHRvSlNPTiBtZXRob2QgYWxsb3dzIHlvdSB0byBwYXJzZSB0aGUgcmVjZWl2ZWQgY29udGVudCB0byBKU09OIG9iamVjdFxuICAgICAgICAgICAgLy8gdmFyIG9iaiA9IHJlc3BvbnNlLmNvbnRlbnQudG9KU09OKCk7XG4gICAgICAgICAgICAvLyBUaGUgdG9JbWFnZSBtZXRob2QgYWxsb3dzIHlvdSB0byBnZXQgdGhlIHJlc3BvbnNlIGJvZHkgYXMgSW1hZ2VTb3VyY2UuXG4gICAgICAgICAgICAvLyB2YXIgaW1nID0gcmVzcG9uc2UuY29udGVudC50b0ltYWdlKCk7XG4gICAgICAgICAgICBhbGVydChgUGV0aWNpw7NuIGVudmlhZGEgJHtzdHJ9YCApO1xuICAgICAgICB9LCAoZSkgPT4ge1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBzZWxlY3RlZEluZGV4Q2hhbmdlZChhcmdzOiBTZWxlY3RlZEluZGV4Q2hhbmdlZEV2ZW50RGF0YSkge1xuICAgICAgICB0aGlzLmJhc2VFbGVnaWRhID0gdGhpcy5pdGVtcy5nZXRJdGVtKGFyZ3MubmV3SW5kZXgpO1xuICAgIH1cblxuICAgIGd1YXJkYXJCYXNlKCkge1xuICAgICAgICBhcHBTZXR0aW5ncy5zZXRTdHJpbmcoJ2xhYmFzZScsIEpTT04uc3RyaW5naWZ5KHRoaXMuYmFzZUVsZWdpZGEpKTtcbiAgICAgICAgVG9hc3QubWFrZVRleHQoJ2Jhc2U6ICcrIHRoaXMuYmFzZUVsZWdpZGEuZGlzcGxheSkuc2hvdygpO1xuICAgICAgICB0aGlzLmxlZXJCYXNlKCk7XG4gICAgfVxuXG4gICAgaW5pdEJhc2VzKCkge1xuICAgICAgICBsZXQgaXRlbXMgPSB0aGlzLm1ha2VCYXNlcygpO1xuICAgICAgICBpdGVtcy5mb3JFYWNoKGkgPT4ge1xuICAgICAgICAgIHRoaXMuaXRlbXMucHVzaChpKTtcbiAgICAgICAgfSk7XG4gICAgICB9XG4gICAgICBcbiAgICAgIG1ha2VCYXNlcygpOiB7dmFsdWU6IHN0cmluZywgZGlzcGxheTogc3RyaW5nfVtdIHtcbiAgICAgICAgcmV0dXJuIFtcbiAgICAgICAgeyB2YWx1ZTogJzEnLCAgZGlzcGxheTogXCJDZW50cmFsIFBhY2h1Y2FcIn0sXG4gICAgICAgIHsgdmFsdWU6ICcyJywgIGRpc3BsYXk6IFwiU29yaWFuYSBEZWwgVmFsbGVcIn0sXG4gICAgICAgIHsgdmFsdWU6ICc1JywgIGRpc3BsYXk6IFwiVmlsbGFzIGRlIFBhY2h1Y2FcIn0sXG4gICAgICAgIHsgdmFsdWU6ICc3JywgIGRpc3BsYXk6IFwiU2FuIEphdmllclwifSxcbiAgICAgICAgeyB2YWx1ZTogJzExJywgZGlzcGxheTogXCJUaXpheXVjYSBDZW50cm9cIn0sXG4gICAgICAgIHsgdmFsdWU6ICc0JywgIGRpc3BsYXk6IFwiVGVjYW1hY1wifSxcbiAgICAgICAgeyB2YWx1ZTogJzYnLCAgZGlzcGxheTogXCJQb3RyZXJvXCJ9LFxuICAgICAgICB7IHZhbHVlOiAnOScsICBkaXNwbGF5OiBcIkxhIDMwMzBcIn0sXG4gICAgICAgIHsgdmFsdWU6ICczJywgIGRpc3BsYXk6IFwiVGl6YXl1Y2EgQ2FycGFcIn0sXG4gICAgICAgIC8vIHsgdmFsdWU6ICcxMCcsIGRpc3BsYXk6IFwiRWwgQ2FybWVuXCJ9LFxuICAgICAgICAvLyB7IHZhbHVlOiAnOCcsICBkaXNwbGF5OiBcIjE4IGRlIE1hcnpvXCJ9LFxuICAgICAgICB7IHZhbHVlOiAnMTInLCBkaXNwbGF5OiBcIkVuY2llcnJvIGRlIFNhbnRvIERvbWluZ28gQXp0YWNhbWVcIn0sXG4gICAgICAgIHsgdmFsdWU6ICcxMycsIGRpc3BsYXk6IFwiTWV0cm8gTWFydGluIENhcnJlcmFcIn0sXG4gICAgICAgIHsgdmFsdWU6ICcxNCcsIGRpc3BsYXk6IFwiU2FuIEJhcnRvbG9tZSBBY3RvcGFuXCJ9LFxuICAgICAgICB7IHZhbHVlOiAnMTUnLCBkaXNwbGF5OiBcIlRlbWFzY2FsYXBhXCJ9LFxuICAgICAgICBdXG4gICAgICB9XG5cbiAgICBsZWVyQmFzZSAoKSB7XG4gICAgICAgIHRoaXMuYmFzZUVsZWdpZGEgPSBKU09OLnBhcnNlKGFwcFNldHRpbmdzLmdldFN0cmluZygnbGFiYXNlJywgJ3tcInZhbHVlXCI6IC0xLCBcImRpc3BsYXlcIjogXCJcIn0nKSk7XG4gICAgICAgIGlmICh0aGlzLmJhc2VFbGVnaWRhICYmIHRoaXMuYmFzZUVsZWdpZGEudmFsdWUgJiYgdGhpcy5iYXNlRWxlZ2lkYS52YWx1ZSA+IDApIHtcbiAgICAgICAgICAgIFRvYXN0Lm1ha2VUZXh0KCB0aGlzLmJhc2VFbGVnaWRhLmRpc3BsYXkpLnNob3coKTtcbiAgICAgICAgICAgIHRoaXMuZXhpc3RlQmFzZSA9IHRydWU7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBiYW5kZXJhem8oKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdCQW5kZXJhem8nKTtcbiAgICAgICAgbG9naW4oe1xuICAgICAgICAgICAgdGl0bGU6IFwiSW5pY2lvXCIsXG4gICAgICAgICAgICBtZXNzYWdlOiBcIlBhcmEgY29udGludWFyIGluZ3Jlc2EgbGEgY2xhdmVcIixcbiAgICAgICAgICAgIG9rQnV0dG9uVGV4dDogXCJPa1wiLFxuICAgICAgICAgICAgY2FuY2VsQnV0dG9uVGV4dDogXCJDYW5jZWxhclwiLFxuICAgICAgICAgICAgcGFzc3dvcmQ6IFwiXCJcbiAgICAgICAgfSkudGhlbigocmVzdWx0KSA9PiB7XG4gICAgICAgICAgICAvLyBUaGUgcmVzdWx0IHByb3BlcnR5IGlzIHRydWUgaWYgdGhlIGRpYWxvZyBpcyBjbG9zZWQgd2l0aCB0aGUgT0sgYnV0dG9uLCBmYWxzZSBpZiBjbG9zZWQgd2l0aCB0aGUgQ2FuY2VsIGJ1dHRvbiBvciB1bmRlZmluZWQgaWYgY2xvc2VkIHdpdGggYSBuZXV0cmFsIGJ1dHRvbi5cbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRGlhbG9nIHJlc3VsdDogXCIgKyByZXN1bHQucmVzdWx0KTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiVXNlcm5hbWU6IFwiICsgcmVzdWx0LnVzZXJOYW1lKTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUGFzc3dvcmQ6IFwiICsgcmVzdWx0LnBhc3N3b3JkKTtcblxuICAgICAgICAgICAgaWYocmVzdWx0LnBhc3N3b3JkID09IFwiMTIzNDU2Nzg5MFwiKSB7XG4gICAgICAgICAgICAgICAgY29uc3Qgb3B0aW9uczogTW9kYWxEaWFsb2dPcHRpb25zID0ge1xuICAgICAgICAgICAgICAgICAgICB2aWV3Q29udGFpbmVyUmVmOiB0aGlzLnZpZXdDb250YWluZXJSZWYsXG4gICAgICAgICAgICAgICAgICAgIGZ1bGxzY3JlZW46IGZhbHNlLFxuICAgICAgICAgICAgICAgICAgICBjb250ZXh0OiB7fVxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgdGhpcy5tb2RhbFNlcnZpY2Uuc2hvd01vZGFsKEJhbmRlcmF6b0NvbXBvbmVudCwgb3B0aW9ucyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH1cbn1cbiIsImltcG9ydCB7IE5nTW9kdWxlLCBOT19FUlJPUlNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdENvbW1vbk1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9jb21tb25cIjtcbmltcG9ydCB7IE5hdGl2ZVNjcmlwdFVJU2lkZURyYXdlck1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktc2lkZWRyYXdlci9hbmd1bGFyXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRVSUxpc3RWaWV3TW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlldy9hbmd1bGFyXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRVSUNhbGVuZGFyTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC11aS1jYWxlbmRhci9hbmd1bGFyXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRVSUNoYXJ0TW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC11aS1jaGFydC9hbmd1bGFyXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRVSURhdGFGb3JtTW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC11aS1kYXRhZm9ybS9hbmd1bGFyXCI7XG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRVSUF1dG9Db21wbGV0ZVRleHRWaWV3TW9kdWxlIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC11aS1hdXRvY29tcGxldGUvYW5ndWxhclwiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0VUlHYXVnZU1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtdWktZ2F1Z2UvYW5ndWxhclwiO1xuaW1wb3J0IHsgTmF0aXZlU2NyaXB0Rm9ybXNNb2R1bGUgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvZm9ybXNcIjtcblxuaW1wb3J0IHsgSG9tZVJvdXRpbmdNb2R1bGUgfSBmcm9tIFwiLi9ob21lLXJvdXRpbmcubW9kdWxlXCI7XG5pbXBvcnQgeyBIb21lQ29tcG9uZW50IH0gZnJvbSBcIi4vaG9tZS5jb21wb25lbnRcIjtcblxuaW1wb3J0IHsgQmFyY29kZVNjYW5uZXIgfSBmcm9tICduYXRpdmVzY3JpcHQtYmFyY29kZXNjYW5uZXInO1xuXG5pbXBvcnQgeyBEcm9wRG93bk1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtZHJvcC1kb3duL2FuZ3VsYXJcIjtcbmltcG9ydCB7QmFuZGVyYXpvQ29tcG9uZW50fSBmcm9tIFwifi9iYW5kZXJhem8vYmFuZGVyYXpvLmNvbXBvbmVudFwiO1xuaW1wb3J0IHtNb2RhbERpYWxvZ1NlcnZpY2V9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9tb2RhbC1kaWFsb2dcIjtcblxuXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtcbiAgICAgICAgTmF0aXZlU2NyaXB0VUlTaWRlRHJhd2VyTW9kdWxlLFxuICAgICAgICBOYXRpdmVTY3JpcHRVSUxpc3RWaWV3TW9kdWxlLFxuICAgICAgICBOYXRpdmVTY3JpcHRVSUNhbGVuZGFyTW9kdWxlLFxuICAgICAgICBOYXRpdmVTY3JpcHRVSUNoYXJ0TW9kdWxlLFxuICAgICAgICBOYXRpdmVTY3JpcHRVSURhdGFGb3JtTW9kdWxlLFxuICAgICAgICBOYXRpdmVTY3JpcHRVSUF1dG9Db21wbGV0ZVRleHRWaWV3TW9kdWxlLFxuICAgICAgICBOYXRpdmVTY3JpcHRVSUdhdWdlTW9kdWxlLFxuICAgICAgICBOYXRpdmVTY3JpcHRDb21tb25Nb2R1bGUsXG4gICAgICAgIEhvbWVSb3V0aW5nTW9kdWxlLFxuICAgICAgICBOYXRpdmVTY3JpcHRGb3Jtc01vZHVsZSxcbiAgICAgICAgRHJvcERvd25Nb2R1bGVcbiAgICBdLFxuICAgIGRlY2xhcmF0aW9uczogW1xuICAgICAgICBIb21lQ29tcG9uZW50LFxuICAgICAgICBCYW5kZXJhem9Db21wb25lbnRcbiAgICBdLFxuICAgIHNjaGVtYXM6IFtcbiAgICAgICAgTk9fRVJST1JTX1NDSEVNQVxuICAgIF0sXG4gICAgcHJvdmlkZXJzOiBbQmFyY29kZVNjYW5uZXIsIE1vZGFsRGlhbG9nU2VydmljZV0sXG4gICAgZW50cnlDb21wb25lbnRzOiBbIEJhbmRlcmF6b0NvbXBvbmVudCBdXG59KVxuZXhwb3J0IGNsYXNzIEhvbWVNb2R1bGUgeyB9XG4iXSwic291cmNlUm9vdCI6IiJ9